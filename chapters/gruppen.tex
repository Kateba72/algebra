\chapter{Gruppen}
\section{Grundlegende Definitionen}
\begin{defn}
	Eine \uline{Gruppe} ist eine Menge \( G \) zusammen mit einer Abbildung \( \cdot : G \times G \to G \) und einem Element \( e \in G \) mit:
	\begin{enumerate}[(i)]
		\item Für alle \( g \in G \) gilt: \( e\cdot g=g \) (e neutrales Element)
		\item Für alle \( g,h,k \in G \) gilt: \( (g\cdot h)\cdot k = g\cdot (h\cdot k) \) (Assoziativität)
		\item Zu jedem \( g\in G \) existiert ein \( h \in G \) mit \( h \cdot g = e \) (inverses Element)
	\end{enumerate}
\end{defn}

\begin{lemma}
	Ist \( G \) eine Gruppe, so gilt:
	\begin{enumerate}[(1)]
		\item \( e \) ist auch rechtsneutral: Für alle \( g \in G \) ist \( g\cdot e=g \) und \( e \) ist eindeutig als linksneutrales Element bestimmt.
		\item Das zu \( g\in G \) inverse Element (wie in (iii)) ist auch rechtsinvers (\( g\cdot h=e \) ) und eindeutig durch \( g \) festgelegt.
	\end{enumerate}
\end{lemma}

\begin{proof}
	zu links \( \leftrightarrow \) rechts: Für \( g \in G \) und linksinverses \( h\in G \) gilt \(h\cdot g=e\). Auch \( h \) hat nach (iii) ein Linksinverses, etwa \( k \in G \). Dann ist \[
		g\cdot h=e\cdot (g\cdot h) = (k\cdot h)\cdot (g\cdot h) = k\cdot (g\cdot h)\cdot h = k\cdot e\cdot h = k\cdot h = e
	.\]
	Ferner ist mit \( g,h,k \) wie oben: \[
	g\cdot e=g\cdot h\cdot g = e\cdot g = g
	.\]

	Eindeutigkeit:\\
	Ist auch \( \tilde{e} \in G \) linksneutral (also nach obigem auch rechtsneutral), so gilt \( e = e \cdot \tilde{e} = \tilde{e} \).\\
	Ist auch \( \tilde{h}\) invers zu \( g \), so ist: \[
		\tilde{h}=\tilde{h}\cdot e= \tilde{h}\cdot (g\cdot h) = (\tilde{h}\cdot g)\cdot h=e\cdot h=h
	.\] 
\end{proof}

Wir schreiben ab jetzt \( g^{-1}\in G \) für das eindeutig bestimmte Inverse zu \( g\in G \).

\begin{lemma}
	In einer Gruppe \( G \) gilt:
	\begin{itemize}
		\item \( (g^{-1})^{-1} = g\;\;  \forall  g \in G \)
		\item \( (gh)^{-1} = h^{-1}\cdot g^{-1} \)
	\end{itemize}
\end{lemma}

\begin{proof}
	Zu zeigen ist: \( g \) (rechte Seite) ist \uline{das} Linkskinverse zu \( (g^{-1}) \). Aber  \[
		g\cdot (g^{-1}) = g\cdot g^{-1} = e
	.\] 
	Zweiter Punkt: Übung
\end{proof}

\begin{defn}
	Eine Gruppe heißt \uline{abelsch}, wenn \[
	gh = hg \; \forall g,h \in G
	.\]
\end{defn}
\begin{bemerkung*}
	Ist \( G \) abelsch, so schreibt man oft \( + \) statt \( \cdot  \). Insbesondere schreibt man auch \( -g \) für das Inverse, \( 0 \) für das neutrale Element und \( m\cdot g \) statt \( g^{m} \).
\end{bemerkung*}
\begin{beispiel*}
	\begin{itemize}
		\item \((\Z, +, 0)\) ist eine albelsche Gruppe.
		\item \( (\Q\setminus \{0\}, \cdot , 1 \) ist eine abelsche Gruppe.
		\item \( GL_n(\Q), \cdot , E_n \) ist eine Gruppe, die für \( n\ge 2 \) nicht abelsch ist.
		\item \( (\faktor{\Z}{2\Z}, +, [0]) \) ist eine abelsche Gruppe mit \( n \) Elementen.
		\item \( (S(n) := \{\sigma: \{1,2,\ldots,n\}\to \{1,2,\ldots,n\} \vert \sigma \text{bijektiv}  \}, \circ, \operatorname{id})  \) ist eine Gruppe mit \( n! \) Elementen, die für \( n \le 2 \) auch abelsch ist.
	\end{itemize}
\end{beispiel*}
\begin{defn}
	Sei \( G \) eine Gruppe. Dann heißt eine Teilmenge \( H \subset  G \) eine \uline{Untergruppe}, wenn
	\begin{enumerate}[{i)}]
		\item \( e \in H \) 
		\item \( g,h \in H \implies gh \in H \)
		\item \( g \in  H \implies g^{-1} \in H \)
	\end{enumerate}
\end{defn}

\begin{defn}
	Eine Gruppe \( G \) heißt \uline{zyklisch}, wenn es ein \( g \in G \) gibt, sodass \( G = \langle g\rangle := \{g^{m}\vert m \in \Z\}  \). Ein solches \( g \) heißt dann \uline{Erzeuger von G}.
\end{defn}

\begin{bemerkung}
	\begin{itemize}
		\item Ist \( G \) zyklisch, so ist \( G \) auch abelsch.
		\item Ist \( \# G < \infty \), so ist auch für \( g \in G \) \( \#\langle g \rangle < \infty \), insbesondere existieren \( m \neq n \) mit \( g^{m} = g^{n} \). Dann ist aber auch (o.B.d.A. \( m > n \)) \( g^{m-n} = g^{n-n}=e \). Also existiert dann ein \( k \in \N \) mit \( g^{k}= e \).
	\end{itemize}
\end{bemerkung}

\begin{lemma}
	Ist \( G \) abelsch oder zyklisch und \( H \subset G \) eine Untergruppe, so ist auch \( H \) abelsch bzw. zyklisch.
\end{lemma}

\begin{proof}
	Die abelsche Aussage ist klar.

	Sei für die zyklische Aussage \( G = \langle g\rangle \) und o.B.d.A. \( H \neq \{e\}  \). Also existiert ein \( k \in  \Z \) mit \( g^{k}\in H \), \( g^{k}\neq e \). Dann auch ein \( g^{k}\in H \) mit \( k \in \N \). Betrachte nun \( r:= \min \{k\in \N \vert g^{k}\in H\} \in \N \).

	Behauptung: \( H = \langle g^{r} \rangle \) \\
	Dazu: Ist ein beliebiges \( g^{n} \in  H \), so betrachte Division mit Rest \[
	n = q\cdot r + s, \quad 0 \le  s < r
	.\] Also \( g^{n}=g^{qr+s}=g^{qr}g^{s} \implies g^{s} = (g^{qr})^{-1} \cdot g^{n} \in H
	.\) Also muss \( s = 0 \) gelten, weil \( r \) minimal war. Daraus folgt \( n = q\cdot r \implies g^{n} \in  \langle g^{r}\rangle \implies H = \langle g^{r}\rangle \)
\end{proof}

\begin{defn}
	Ist \( G \) eine Gruppe, \( g \in  G \), so heißt \( \operatorname{ord}(g) := \# \langle g\rangle \in \N \cup \{\infty\} \) die \uline{Ordnung des Elements \( g \)}.
\end{defn}

\section{Gruppenquotienten}
Sei \( G \) eine Gruppe, \( h \in G \) eine Untergruppe. Sei \( g\cdot H := \{g\cdot h|h \in H\} \ni g = g\cdot e \). Ist \( g \cdot H \cap g'H \neq \emptyset \), so ist \( gH = g'H \), denn ist \( x \in gH \cap g'H \), so existieren \( h, h' \in H \) mit \[
	gh=x=g'h' \implies gh = g'h' \implies g' = gh(h')^{-1}
.\] Damit: \( gH \subset g'H \), denn sei \( g\cdot k \in gH\), dann ist \( gk=g'h'h^{-1}k \in g'H \)
und \( g'H \subset gH \), denn sei \( g'\cdot k \in g'H\), dann ist \( g'k=gh(h')^{-1}k \in gH \)

Obiges zeigt: Setzt man \( g \sim g' :\Leftrightarrow (g')^{-1}g\in H \), so ist das eine Äquivalenzrelation.

\begin{defn}
	Ist \( G \) eine Gruppe, \( H \subset  G \) eine Untergruppe, so ist der \uline{Gruppenquotient} \( \faktor{G}{H} \) die Menge \[
		\faktor{G}{H} = \{g\cdot H | g \in G\}
	.\] Man schreibt \( [g] := g\cdot H \) und nennt das die \uline{Nebenklasse} von \( g \).

	Es ergibt sich eine natürliche Abbildung
	\begin{align*}
		\pi: G &\longrightarrow \faktor{G}{H} \\
		g &\longmapsto g\cdot H
	.\end{align*}
\end{defn}
\begin{defn}
	Sei \( H\subset G \) eine Untergruppe, \( [g] \in  \faktor{G}{H} \). Dann heißt jedes \( \tilde{g}\in G \) mit \( [\tilde{g}]=[g] \) \uline{Repräsentant von [g]}. Ein \uline{Repräsentantensystem} ist eine Teilmenge \( R \subset G \), so dass für jedes \( [g] \in \faktor{G}{H} \) genau ein Repräsentant \( \tilde{g}\in [g] \) existiert mit \( \tilde{g}\in R \).
\end{defn}

\begin{lemma}
	Ist \( H \subset G \) eine Untergruppe und \( R \subset G \) ein Repräsentantensystem, so ist \[
	G = \bigdotcup_{g \in R} gH
	.\] 
\end{lemma}

\begin{korollar}
	Ist \( \# G < \infty \) (und damit auch \( \#H < \infty \) ), so gilt: \[
	\#G = \#R \cdot \#H = \#\faktor{G}{H} \cdot \#H
	.\] 
\end{korollar}

\begin{proof}
	Für \( g, g' \in G \) ist 
	\begin{align*}
		gH &\longrightarrow g'H \\
		g\cdot h &\longmapsto g'\cdot H = g'g^{-1}\cdot (gh)
	.\end{align*}
	eine Abbildung zwischen zwei endlichen Mengen mit Umkehrfunktion
	\begin{align*}
		g'H &\longrightarrow gH \\
		g'\cdot h &\longmapsto g\cdot H = g(g')^{-1}\cdot (g'h)
	,\end{align*} also eine Bijektion.
	Also ist \( \#gH = \#g'H \) für alle \( g, g' \in  G \), insbesondere also \( g, g' \in R \).
\end{proof}

\begin{satz}[Satz von Lagrange]
	Ist \( H \subset G \) eine Untergruppe einer endlichen Gruppe \( G \), so folgt \( \#H \vert \#G \).
\end{satz}
\begin{korollar}
	Ist \( \#G < \infty\) und \( g \in G \), so gilt \( \operatorname{ord}(g) \vert \#G  \). 
\end{korollar}

\begin{korollar}
	Ist \( g\in G, \#G < \infty \), so ist \( g^{\#G}=e \).
\end{korollar}

Frage: Trägt \( \faktor{G}{H} \) wieder eine natürliche Gruppenstruktur?

Ansatz: Probiere  \( [e] \) als neutrales Element und als Verknüpfung
\begin{align*}
	\faktor{G}{H}\times \faktor{G}{H} &\longrightarrow \faktor{G}{H} \\
	([g],[g']) &\longmapsto [g\cdot g']
.\end{align*}

Zur Wohldefiniertheit: Sind \( g_1,g_2\in [g], g_1', g_2' \in [g'] \), so liefert Wahl 1 als Ergebnis \( [g_1g_1'] \) und Wahl 2 als Ergebnis \( [g_2g_2'] \). Ist \( g_1g_1'\sim g_2g_2' \)?

Wegen \( g_1,g_2\in [g] \), d.h. \( g_1\sim g_2 \) ex. ein \( h\in H \) mit \( g_2=g_1\cdot h \). Analog existiert ein \( h'\in H \) mit \( g_2' = g_1'h' \). Also ist \( g_2g_2' = g_1hg_1'h' = g_1g_1'\underbrace{(g_1')^{-1}hg_1'h'}_{\in H?} \) 

Im Allgemeinen gilt das nicht, d.h. \( \faktor{G}{H} \) trägt im Allgemeinen keine natürliche Gruppenstruktur.

\begin{defn}
	Eine Untergruppe \( H \subset  G \) heißt ein \uline{Normalteiler}, wenn für alle \( g \in  G \) \( gHg^{-1} \in H \).
\end{defn}

\begin{satz}
	Ist \( H \subset G \) ein Normalteiler, so ist durch
	\begin{align*}
		\cdot :\faktor{G}{H}\times \faktor{G}{H} &\longrightarrow \faktor{G}{H} \\
		([g], [g']) &\longmapsto [gg']
	\end{align*} als Verknüpfung und \( [e] \) als neutralem Element eine Gruppenstruktur auf \( \faktor{G}{H} \) definiert.
\end{satz}
\begin{proof}
	siehe obige Rechnung.
\end{proof}

\begin{bemerkung}
	Ist \( G \) abelsch, so ist jede Untergruppe \( H \subset G \) ein Normalteiler.
\end{bemerkung}

\section{Gruppenhomomorphismen}

\begin{defn}
	Eine Abbildung \( f: G \to H \) heißt \uline{Gruppenhomomorphismus}, wenn
	\begin{enumerate}[{i)}]
		\item \( f(e_G) = e_H\)
		\item \( f(g\cdot_G g' = f(g) \cdot_H f(g')\)
	\end{enumerate}
\end{defn}

\begin{beispiel}
	\( f: \: \faktor{\Z}{n\Z}\to \mu_n,\, [a]\mapsto e^{\frac{2\pi ia}{n}} \) ist ein Gruppenhomomorphismus, denn
	\begin{enumerate}[{i)}]
		\item \( f([0]) = e^{\frac{2\pi i0}{n}} = e^{0} = 1 \) 
		\item \( f([a+b]) = e^{\frac{2\pi i(a+b)}{n}} = e^{\frac{2\pi ia}{n}}\cdot e^{\frac{2\pi ib}{n}} = f([a]) + f([b]) \)
	\end{enumerate}
\end{beispiel}

\begin{lemma}
	Ist \( f: G \to  H \) ein Homomorphismus, so ist für alle \( g \in G \) \[
		f(g^{-1}) = f(g)^{-1}
	.\] 
\end{lemma}
\begin{proof}\(
		f(g)^{-1} = f(g)^{-1}f(e) = f(g)^{-1}f(g)f(g^{-1}) = f(g^{-1})
.\) 
\end{proof}

\begin{defn}
	Für einen Gruppenhomomorphismus \( f:G\to H \) heißt \[
		\operatorname{ker}( f ) := \{g\in G | f(g) = e\} 
	.\] der \uline{Kern} von \( f \).
\end{defn}
\begin{lemma}
	In obiger Situation ist \( \operatorname{ker}( f ) \subset G \) ein Normalteiler.
\end{lemma}
% 24. 10. 2019
\begin{proof}
	\( \operatorname{ker}f \subset G  \) ist eine Untergruppe, denn:
	\begin{itemize}
		\item \( e \in \operatorname{ker}f  \), weil \( f(e) = e \)
		\item Ist \( g, g' \in  \operatorname{ker}f \), so ist \[
			f(gg') = f(g) f(g') = e\cdot e = e \implies gg' \in \operatorname{ker}f 
		.\] 
	\item \( g \in  \operatorname{ker} f \implies f(g^{-1}) = f(g)^{-1}= e^{-1}=e \implies g^{-1}\in \operatorname{ker} f\) 
	\end{itemize}

	\( \operatorname{ker}f \subset G  \) ist sogar ein Normalteiler, denn:\\
	Zu zeigen: \( \forall g \in G: \: g(\operatorname{ker}f  g^{-1} \subset \operatorname{ker}f)\)\\
	Dazu: Sei \( g \in  G \) und \( x \in  \operatorname{ker} f \). Dann \( f(gxg^{-1}) = f(g)f(x)f(g)^{-1} = f(g) f(g)^{-1} = e \) \( \implies gxg^{-1} \in \operatorname{ker} f \)
\end{proof}

\begin{bemerkung*}
	\begin{itemize}
		\item Wir wissen damit, dass \( \faktor{G}{\operatorname{ker} f} \) in natürlicher Weise eine Gruppe ist.
		\item Notation: Ist \( N \) ein Normalteiler in \( G \), schreibt man oft \( N \vartriangleleft G \)
	\end{itemize}
\end{bemerkung*}

\begin{defn}
	Für eine Untergruppe \( H \subset G \) (\( G \) nicht notwendigerweise endlich), heißt \[
		\#\faktor{G}{H} =: [G:H] \in \N \cup \{\infty\} 
	.\] der \uline{Index von \( H \) in \( G \).}
\end{defn}
\begin{bemerkung}
	Ist \( \#G<\infty \), so ist \( [G:H] = \frac{\#G}{\#H} \).
\end{bemerkung}

\begin{beispiel}[für Gruppen]
	Die Diedergruppe \( D_n \) ist die Gruppe, die von den Rotationen und Spiegelungen des \( \R^2 \) erzeugt wird, die das regelmäßige \( n \)-Eck invariant lassen.

	Sei \( r:= \text{Drehung um } \frac{2\pi}{n} \), \( s \) eine Spiegelung. Schreibe \[
	D_n = \langle r,s | r^{n} = e,\, s^2 = e,\, s rs=r^{-1} \rangle 
	.\] 
	In dieser Notation nennt man \( r, s \) \uline{freie Erzeuger}, d.h. jedes Element schreibt sich als \uline{Wort} in den zwei Buchstaben \( r,s \): \[
	g \in D_n \leadsto g = r^{a_1}s^{b_1}r^{a_2}s^{b_2}\ldots r^{a_n}s^{b_n}, \quad a_i, b_i \in \Z
	.\]
	\( e \) ist das leere Wort \( r^{0}s^{0} \).

	Die Relationen \( r^{n}=e, s^2= e, s rs = r^{-1} \) sorgen dafür, dass nicht alle Wörter paarweise verschieden sind. Genauer: \( Wort_1=Wort_2 \), wenn \( Wort_1 \) aus \( Wort_2 \) durch sukzessives (endliches) Anwenden der Relationen ensteht.

	Also ist
	\begin{align*}
		D_n &= \{e, r, r^2, \ldots, r^{n-1}, s, s r, s r^2, \ldots, s r^{n-1} \} \\
		    &= \{e, s, r, rs, r^2, r^2s, \ldots, r^{n-1}, r^{n-1}s\}
	.\end{align*}
	Die Gruppe \( D_n \) hat \( 2n \) Elemente. Beachte dazu, dass \( r^{a_1}s^{b_1}\ldots r^{n}s^{n} \) in \( D_n \) immer als \( r^{A}s^{B},\, A,B \in \Z \) geschrieben werden kann.
	
	Betrachte die Untergruppe \[
	H := \langle r \rangle = \{r^{k}| k\in \Z\} = \{e, r, r^2, \ldots, r^{n-1}\} 
	.\]
	\( H \vartriangleleft D_n \), denn für \( g \in D_n \) (in der Form \( s^{a}r^{b},\, a,b \in \N_0 \) und \( r^{k} \in H \)) ist
	\[ gr^{k}g^{-1} = s^{a}r^{b}r^{k}r^{-b}s^{-a}= s^{a}r^{k}s^{-a} = \begin{cases}
			r^{k}, &\quad a \text{ gerade},\\
			s r^{k}s^{-1}, &\quad a \text{ ungerade}
		\end{cases}
		\]
	Aber \( s r^{k} s^{-1} = (s rs^{-1})^{k} \) und \( s rs^{-1} = s rs = r^{-1} = r^{n-1} \in  H \).

	\uline{Übungsaufgabe}: \( \langle s \rangle \subset D_n \) ist kein Normalteiler für \( n \ge 3 \)
\end{beispiel}

\begin{lemma}
	Sei \( \varphi: G\to H \) ein Homomorphismus. Es gilt \[
		\varphi \text{ injektiv } \Leftrightarrow \operatorname{ker}\varphi = \{e\}
	.\] 
\end{lemma}
\begin{proof}
	"\(\implies \)": Ist \( \varphi \) injektiv und \( x \in \operatorname{ker}\varphi \), so folgt \( \varphi(x)=e = \varphi(e) \implies x = e \)\\
	"\( \Longleftarrow \)": Ist  \( \operatorname{ker}\varphi = \{e\}  \) und \( x, y \in G \) mit \( \varphi(x) = \varphi(y). \) Dann ist \( e = \varphi(x) \varphi(x)^{-1} = \varphi(x) \varphi(y) = \varphi(x)\varphi(y^{-1}) = \varphi( x\cdot y^{-1})\) \( \implies xy^{-1}= e \implies x = y \)
\end{proof}

\begin{satz}[Homomorphiesatz]\label{satz:homomorphiesatz_gruppen}
	Ist \( \varphi: G\to H \) ein Gruppenhomomorphismus, so induziert \( \varphi \) einen wohldefinierten Gruppenhomomorphismus 
	\begin{align*}
		\overline{\varphi}: \faktor{G}{\operatorname{ker}\varphi} &\longrightarrow H \\
		[g] &\longmapsto \overline{\varphi}([g]) := \varphi(g)
	.\end{align*}
	und dieser ist injektiv.
\end{satz}

\begin{proof}
	\( \overline{\varphi} \) ist wohldefiniert, denn seien \( g, g' \in [g] \), so existiert ein \( y \in \operatorname{ker}\varphi \) mit \( g'=gy \). Dann ist  \[
		\varphi(g') = \varphi(gy) = \varphi(g)\varphi(y) = \varphi(g)e=\varphi(g)
	.\] 
	\( \overline{\varphi} \) ist ein Gruppenhomomorphismus, denn
		\begin{itemize}
			\item \(\overline{\varphi}([g]\cdot [g']) = \overline{\varphi}([gg']) = \varphi(gg') = \varphi(g)\cdot \varphi(g') = \overline{\varphi}([g]) \overline{\varphi}([g'])\)
			\item \( \overline{\varphi}([e]) = \varphi(e) = e \)
		\end{itemize} 
	\( \overline{\varphi} \) ist injektiv, denn 
	\begin{align*}
		\operatorname{ker}\overline{\varphi}&=\{[g] \in  \faktor{G}{\operatorname{ker}\varphi}|\overline{\varphi}([g]) = e\} = \{[g]\in \faktor{G}{\operatorname{ker}\varphi}| \varphi(g) = e\}\\ 
						    &= \{[g] \in  \faktor{G}{\operatorname{ker}\varphi}| g \in \operatorname{ker}\varphi\} = \{e\} 
	.\end{align*}
\end{proof}

\begin{defn}
	Ein Gruppenhomomorphismus \( \varphi: G \to H \) heißt \uline{Isomorphismus}, wenn es einen Gruppenhomomorphismus \( \psi: H \to G \) gibt, so dass \[
		(\varphi \circ \psi: H \to G \to H) = \operatorname{id}_H
	.\] und \[
	(\psi \circ \varphi: G \to H \to G) = \operatorname{id}_G
	.\] 
\end{defn}

\begin{bemerkung}
	\begin{enumerate}[{(1)}]
		\item Ist \( \varphi \) ein Isomorphismus, so ist \( \varphi \) bijektiv
		\item Umgekehrt gilt auch: Ist \( \varphi: G\to H \) ein bijektiver Gruppenhomomorphismus, so ist \( \varphi \) ein Isomorphismus.
	\end{enumerate}
\end{bemerkung}

\uline{zu (2)}: Da \( \varphi \) bijektiv, existiert eine Umkehrabbildung \( \psi: H\to G \) als Mengenabbildung. \( \psi \) ist automatisch ein Gruppenhomomorphismus, denn \( \varphi(e) = e \implies \psi(e) = e \) und \( \varphi(g\cdot h) = \varphi(g) \cdot  \varphi(h) \implies \psi(\sigma\cdot \tau) = \psi(\varphi(g)\cdot \varphi(h)) = g\cdot h = \psi(\sigma) = \psi(\tau)  \)

\begin{satz}
	Ist \( \varphi: G\to H \) ein Homomorphismus, so ist \[
		\overline{\varphi}: \faktor{G}{\operatorname{ker}\varphi} \to \operatorname{Im}\varphi := \{\varphi(g) \in H | g \in G\} 
	.\] ein Gruppenisomorphismus.

	Insbesondere: Ist \( \varphi: G\to H \) ein surjektiver Homomorphismus, so ist \( \overline{\varphi}: \faktor{G}{\operatorname{ker}\varphi}\to H \) ein Isomorphismus.
\end{satz}

Beweis siehe Übung.

Schreibweise: \( \varphi: G \overset{\cong}{\longrightarrow} H :\iff \varphi \text{ Isomorphismus} \).

\begin{beispiel}
	\( \varphi: \Z \to \mu_n, a \mapsto e^{\frac{2\pi i a}{n}} \) ist surjektiver Homomorphismus mit \( \operatorname{ker}\varphi = n\Z \vartriangleleft \Z \) \[
		\implies \overline{\varphi}: \faktor{\Z}{n\Z} \overset{\cong}{\longrightarrow} \mu_n
	.\] 
\end{beispiel}

\section{Letzte Bemerkungen zu Gruppen}

\begin{defn}
Ist \( G \) eine Gruppe und \( g \in G \) fixiert, so ist die  \uline{Konjugation mit \( g \)} der Gruppenisomorphismus
\begin{align*}
	\operatorname{conj}_g: G &\longrightarrow G \\
	x &\longmapsto \operatorname{conj}_g(x) = gxg^{-1}
.\end{align*}
\end{defn}

Beachte:
\begin{itemize}
	\item \( \operatorname{conj}_g(e) = geg^{-1} = e \)
	\item \( \operatorname{conj}_g(xy) = gxyg^{-1} = gxg^{-1}gyg^{-1}=\operatorname{conj}_g(x)\operatorname{conj}_g(y) \) 
	\item \( \operatorname{conj}_g \) ist ein Isomorphismus mit Umkehrabbildung \( \operatorname{conj}_{g^{-1}} \)
\end{itemize}

\begin{defsatz}
Die Menge \( \operatorname{Aut}G := \{\varphi: G\to G | \varphi \text{ Isomorphismus}\}  \) der \uline{Automorphismen} mit \( \operatorname{id}_G \) als neutralem Element und der Komposition \( \circ \) ist eine Gruppe.

Weiterhin ist \begin{align*}
	\gamma: G &\longrightarrow \operatorname{Aut}G \\
	g &\longmapsto \gamma(g) := \operatorname{conj}_g
.\end{align*} ein Gruppenhomomorphismus.\\
\end{defsatz}

\begin{proof}
	\begin{itemize}
		\item \( \gamma(e) = id_G \), weil \( \gamma(e)(x) = \operatorname{conj}_e(x) = exe^{-1} = x = id_G(x) \) für alle \( x \in G \).
		\item \( \gamma(g\cdot \gamma')(x) = \operatorname{conj}_{gg'}(x) = (gg')x(gg')^{-1} = gg'xg'^{-1}g^{-1} = \operatorname{conj}_g \circ \operatorname{conj}_{g'}(x) \) für alle \( x \in G \).
	\end{itemize}
\end{proof}

\begin{bemerkung}
	Ist \( G \) eine endliche Gruppe mit \( n:=\#G \), so existiert ein  injektiver Gruppenhomomorphismus \[
		\varphi: G \to S(n)
	.\] 
Insbesondere ist \( G \) also isomorph zu einer Untergruppe \( \operatorname{Bild}\varphi \subset S(n) \)
\end{bemerkung}
\begin{proof}
	Wir schreiben \( G = \{g_1, \ldots, g_n\} \), dann induziert jedes \( g \in G \) eine Mengenabbildung 
	\[
		\begin{tikzcd}
			G \arrow[d] & g_1 \arrow[d, maps to] & g_2 \arrow[d, maps to] & \ldots & g_n \arrow[d, maps to] \\
			G & g\cdot g_1 & g\cdot g_2 & & g\cdot g_n
		\end{tikzcd}
	\] 
	und damit ein \( \sigma \in S(n) \). Dies induziert \( G\to S(n) \).
\end{proof}

Ob das nützlich ist?
Ist \( G = S(n) \), so liefert diese Bemerkung einen injektiven Homomorphismus in die Gruppe \( S(n!) \) mit \( (n!)! \) Elementen.

\begin{defn}
	Eine Gruppe \( G \) heißt \uline{einfach}, wenn sie nur \( \{e\}  \) und \( G \) als Normalteiler hat.
\end{defn}

\begin{bemerkung}
Ist \( G \) einfach und \( \varphi: G\to H \) ein Homomorphismus, so ist \( \varphi \) entweder konstant oder injektiv.
\end{bemerkung}

Hoffnung: Wir wollen \( G \) verstehen, \( G \) hat einen Normalteiler \( N \vartriangleleft G\), \( \{e\} \neq  N \neq G \) . Dann hat man \[
	G \overset{\pi}{\longrightarrow} \faktor{G}{N}, \quad g \mapsto [g]
.\] mit \( \operatorname{ker}\pi = N \).

Hoffnung: Versteht man \( N \), (\( \#N | \#G \) ) und \( \faktor{G}{N} \) gut, so hoffentlich auch \( G \). Im Allgemeinen leider nicht!

Gibt es genügend viele einfache Gruppen? Ja. Es gibt ganz "`exotische"' einfache Gruppen, zum Beispiel die "`Monstergruppe"' oder die "`Babymonstergruppe"'. \( \#Monstergruppe \approx 8\cdot 10^{53} \)

\begin{lemma}
	Es existiert ein eindeutig bestimmter Gruppenhomomorphismus \[
		\operatorname{sgn}: S(n) \to  \mu_2 = \{\pm 1\}
	,\] so dass \( \operatorname{sgn}((a\,b)) = -1 \) für jeden 2-Zykel \( (a\,b) \in S(n) \).
\end{lemma}
\begin{proof}

	\uline{Existenz}: Definiere
	\begin{align*}
		\Delta : \Z^{n} \to  \Z, \quad & (x_1,\ldots, x_n) \mapsto \prod_{i < j} (x_j - x_i) \\  
		\sigma \Delta : \Z^{n} \to  \Z, \quad & (x_1,\ldots, x_n) \mapsto \prod_{i < j} (x_{\sigma(j)} - x_{\sigma(i)})   
	.\end{align*}
	Es gilt \( \abs{\Delta(x)} = \abs{\sigma\Delta(x)} \) für alle \( x \in \Z^{n} \). Dann definiere \( \operatorname{sgn}(\sigma) \in \{\pm 1\}  \) mit \[
		\sigma\Delta = \operatorname{sgn}(\sigma) \cdot  \Delta
	.\] \uline{Gruppenhomomorphismus}: \( \operatorname{sgn} \) ist ein Gruppenhomomorphismus, denn \[
	(\sigma\tau)\Delta(x) = \prod_{i<j} (x_{\sigma\tau(j)} - x_{\sigma\tau(i)}) = \operatorname{sgn}(\sigma) \cdot \prod_{i<j} (x_{\tau(j)}- x_{\tau(i)}) = \operatorname{sgn}(\sigma) \cdot \operatorname{sgn}(\tau) \cdot \Delta(x)
.\] \uline{Eindeutigkeit}: Jedes \( \sigma \in S(n) \) lässt sich schreiben als Produkt von Transpositionen
\end{proof}

\begin{defn}
	Für \( n \in \N \) heißt \[
		A(n) := \operatorname{ker}(\operatorname{sgn}: S(n) \to \mu_2)
	.\] die alternierende Gruppe zu \( n \in \N \).
\end{defn}

\begin{bemerkung}
	\begin{itemize}
		\item \( A(n) = \{\sigma \in  S(n) | \operatorname{sgn}\sigma = 1\} \vartriangleleft S(n) \) ist ein Normalteiler.
		\item später: \( A(n) \) ist für \( n \ge 5 \) einfach.
		\item Isomorphiesatz: Für \( n \ge 2 \) ist \( \faktor{S(n)}{A(n)} \overset{\cong}{\to} \mu_2 \). Also ist \( [S(n): A(n)] = \# \faktor{S(n)}{A(n)} = 2 \) und \( \#A(n) = \frac{n!}{2} \).
	\end{itemize}
\end{bemerkung}

\begin{defn}
	Eine \uline{kurze exakte Sequenz} von Gruppen ist eine Sequenz \[
		1 \to A \overset{\alpha}\to B \overset\beta\to C \to 1
	.\] von Gruppenhomomorphismen, wobei \( 1:= \{e\}  \), mit \( \beta\circ\alpha = \operatorname{const}_1 \) und sogar:
	\begin{itemize}
		\item \( \operatorname{ker}\beta = \operatorname{Bild}\alpha \)
		\item \( \alpha \) injektiv
		\item \( \beta \) surjektiv.
	\end{itemize}
\end{defn}

\begin{lemma}
Ist \( 1 \to A \overset\alpha\to B \overset\beta\to C \to 1 \) eine kurze exakte Sequenz, so hat man einen Isomorphismus \[
\faktor{B}{\operatorname{Bild}\alpha} \overset\cong\longrightarrow C
.\] und \( A \overset{\alpha, \cong}\longrightarrow \operatorname{Bild}\alpha \) ist ein Isomorphismus. Letzteres sagt: Wir können \( A \) mit \( \operatorname{Bild}\alpha \) identifizieren und \( \faktor{B}{A} \) für \( \faktor{B}{\operatorname{Bild}\alpha} \) schreiben.
\end{lemma}

\begin{satz}[Erster Noetherscher Isomorphiesatz]
	Sei \( G \) eine Gruppe, \( U \subset G \) Untergruppe und \( N \vartriangleleft G \) ein Normalteiler. Dann ist \( U\cdot N := \{u\cdot n | u \in U, n \in N\}  \) auch eine Untergruppe und man hat eine kurze exakte Sequenz 
\begin{align*}
	1 \to U \cap N \to &U \to \faktor{U\cdot N}{N} \to 1& \\
			   & u \mapsto [u\cdot 1] = u\cdot N&
.\end{align*}
Insbesondere hat man einen Isomorphismus \[
\faktor{U}{U \cap N} \overset\cong\to \faktor{UN}{N}
.\] 
\end{satz}

\begin{proof}
	\( UN \) ist Untergruppe, denn \( 1\cdot 1 = 1 \in U\cdot N \); für \( u\cdot n, v\cdot m \in U\cdot N \) ist \( unvm = uv\underbrace{v^{-1}nv}_{\in N}m \in  U\cdot N \). Ferner ist \( (un)^{-1} = n^{-1}u^{-1} = u^{-1}\underbrace{un^{-1}u^{-1}}_{\in N} \in U\cdot N \). \\
	\( \varphi: U \to \faktor{UN}{N} \) ist surjektiv, denn sei \( [u\cdot n] = [u] = \varphi(u) \). \( \operatorname{ker}\varphi = \{u \in U | [u] = [1]\} = \{u \in U | u \in  N\} = U \cap N  \).
\end{proof}

\begin{satz}[Zweiter Noetherscher Isomorphiesatz] 
	Sei \( G \) eine Gruppe, \( K,H \vartriangleleft G \) mit  \( K \subset H \). Dann hat man eine kurze exakte Sequenz
	\begin{align*}
		1 \to \faktor{H}{K}  \to  \faktor{G}{K} &\to \faktor{G}{H} \to  1 \\
					  [g] &\mapsto [g]
	.\end{align*}
	Insbesondere hat man einen Isomorphismus \[
	\faktor{\faktor{G}{K}}{\faktor{H}{K}} \overset\cong\to \faktor{G}{H}
	.\] 
\end{satz}
