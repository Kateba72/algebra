\chapter{Ringe}
\section{Grundlagen}

\begin{erinnerung*}
	Ein kommutativer Ring mit 1 ist ein Tupel \( (R, +, \cdot ) \), wobei \( (R,+) \) eine abelsche Gruppe ist. Außerdem ist \( \cdot  \) assoziativ, es gibt ein neutrales Element von \( \cdot  \), genannt \( 1 \) und \( \cdot  \) ist kommutativ. Weiterhin gelte das Distributivgesetz \( (x+y)\cdot z = xz + yz \)
\end{erinnerung*}

Ab hier sei ein "`Ring"' ein kommutativer Ring mit 1.

\begin{beispiel*}
	\begin{itemize}
		\item \( R = \Z \) 
		\item \( R=K[X] \),  \( K \) Körper
		\item \( K \) Körper \( \implies K \) Ring
	\end{itemize}
\end{beispiel*}

\begin{bemerkung*}
	\( K \) Körper \( \iff K \) Ring und \( \forall  x\neq 0 \exists  y \in K: xy=1 \)
\end{bemerkung*}

\begin{defn}
	Ist \( R \) ein Ring, so heißt \[
		R^\times := \{x\in R \mid \exists y \in R: xy=1\}
	.\] die \uline{Einheitengruppe} von \( R \).
\end{defn}

\begin{bemerkung}
	\( (R^\times, \cdot , 1) \) ist eine abelsche Gruppe.
\end{bemerkung}

\begin{beispiel}
	\begin{itemize}
		\item \( \Z^\times = \{-1, 1\}  \) 
		\item \( K \) Körper \( \implies K^\times = K \setminus \{0\}  \) (Beachte: In jedem Ring gilt \( 0\cdot x = 0 \) für alle \( x \in R \) und im Körper ist \( 0 \neq 1 \).)
		\item \( K[X]^\times = K^\times \), denn ist \( f(X) \in K[X]^\times  \), so ex. per Def ein \( g(X) \in K[X] \) mit \( 1 = f(X)\cdot g(X) \). Mit  \( 0 = \operatorname{deg}(fg) = \operatorname{deg}f + \operatorname{deg}g \) folgt dann \( \operatorname{deg}f = 0 \).
	\end{itemize}
\end{beispiel}

\begin{defn}
	Seien \( R, S \) zwei Ringe, dann heißt \( \varphi: R \to S \) ein \uline{Ringhomomorphismus}, wenn 
	\begin{enumerate}[{i)}]
		\item \(\varphi(x+y) = \varphi(x) + \varphi(y)\)
		\item \(\varphi(xy) = \varphi(x)\varphi(y)\)
		\item \(\varphi(0) = 0, \varphi(1) = 1\)
	\end{enumerate}
	Wir schreiben \( \operatorname{Hom}(R,S):= \{\varphi: R\to S \mid \varphi \text{ Ringhomo}\}  \) 
\end{defn}

\begin{bemerkung}
	Ist \( \varphi \in \operatorname{Hom}(R,S)\), so ist \( \varphi: (R, +, 0) \to  (S, +, 0) \) ein Gruppenhomomorphismus abelscher Gruppen.
	
	Insbesondere ist \[
		\operatorname{ker}\varphi:= \{x\in R \mid \varphi(x) = 0\} \subset R
	.\] eine Untergruppe und \( \varphi \) ist genau dann injektiv, wenn \( \operatorname{ker}\varphi=\{0\}  \)
\end{bemerkung}

\begin{defn}
	Ein \uline{Ideal} in \( R \) ist eine Teilmenge \( \mathfrak{a}\subset R \), so dass
	\begin{enumerate}[{i)}]
		\item \( 0 \in \mathfrak{a} \) 
		\item \( x, y \in  \mathfrak{a} \implies x+y \in \mathfrak{a} \) 
		\item Für \( a \in \mathfrak{a},\, x \in R \) ist \( x\cdot a \in \mathfrak{a} \) 
	\end{enumerate}
	Wir schreiben dann \( \mathfrak{a} \vartriangleleft R \).
\end{defn}

\begin{bemerkung}
	Es folgt dann auch, dass \( x \in \mathfrak{a} \implies -x = (-1)\cdot x \in \mathfrak{a} \)
\end{bemerkung}

\begin{lemma}
	Ist \( \varphi: R\to S \) ein Ringhomomorphismus, so ist \( \operatorname{ker}\varphi \vartriangleleft R \) ein Ideal.
\end{lemma}

\begin{proof}
	\begin{itemize}
		\item \( \varphi(0) = 0 \implies 0 \in  \operatorname{ker}\varphi \)
		\item \( x, y \in  \operatorname{ker}\varphi, \) so folgt \( \varphi(x+y) = \varphi(x)+ \varphi(y) = 0+0= 0 \implies x+y \in  \operatorname{ker}\varphi \).
		\item Ist \( a \in  \operatorname{ker}\varphi \) und \( x \in R \) beliebig. Dann ist \( \varphi(xa) = \varphi(x)\varphi(a) = \varphi(x)\cdot 0 = 0 \implies xa \in \operatorname{ker}\varphi \).
	\end{itemize}
\end{proof}

\begin{beispiel}
	\begin{itemize}
	\item Die Funktion
	\begin{align*}
		\varphi: \Z &\longrightarrow \Z \\
		x &\longmapsto n\cdot x
	.\end{align*} ist für ein festes \( n \in \Z \) kein Ringhomomorphismus für \( n \neq - 1 \).
\item \( \mathfrak{a}:= n\Z\), \( n \in Z \) fest gewählt. Dann ist \( \mathfrak{a} \vartriangleleft \Z \), denn für \( a \in \mathfrak{a} \) (also \( a = n\cdot b \) für ein \( b \in Z \) ) und \( x \in \Z \), so ist \( xa = nxb \in \mathfrak{a} \)
        \end{itemize}
\end{beispiel}

\begin{defn}
	Ist \( R \) ein Ring und \( a \in R \) beliebig, so heißt \[
		(a) := Ra := \{xa \in R \mid x \in \R\} 
	.\] das \uline{Hauptideal} von \( a \) in \( R \).

	Allgemeiner: Sind \( a_1, \ldots, a_n \in R \), so heißt \( (a_1, \ldots, a_n) := \{x_1a_1 + \ldots + x_na_n \in R\mid x_1, \ldots, x_n \in  R\} \vartriangleleft R  \) das von \( (a_1, \ldots, a_n) \) erzeugte Ideal.
\end{defn}

\begin{beispiel}
	\begin{itemize}
		\item In \( \Z \) ist \( (n) = n\Z \vartriangleleft Z \)
		\item Ist \( L | K \) eine Körpererweiterung und \( \alpha \in L \) algebraisch, dann ist \[
				\mathfrak{a}:= \{f(X) \in K[X] \mid f(\alpha) = 0\} \vartriangleleft K[X]
			,\] denn
			\begin{align*}
				K[X] &\longrightarrow K \\
				\varphi &\longmapsto \varphi(\alpha)
			\end{align*} ist ein Ringhomomorphismus.
	\end{itemize}
\end{beispiel}

Beachte:
	Ist \( p(X) \in K[X] \) das Minimalpolynom von \( \alpha \) über \( K \), so ist \( \mathfrak{a} = (p(X)) \vartriangleleft K[X] \).

\begin{lemma}
	Ist \( \mathfrak{a}\vartriangleleft R \) und \( \mathfrak{a}\cap R^\times \neq \emptyset \), so ist \( \mathfrak{a}=R \).
\end{lemma}

\begin{proof}
	Ist \( x \in  \mathfrak{a}\cap R^\times \), so ex. \( y \in R \) mit \( xy = 1 \). Also ist \( 1 = y\cdot x \in \mathfrak{a} \). Für beliebige \( z \in R \) ist aber  \[
	z = z\cdot 1 \in  \mathfrak{a}
	.\] 
\end{proof}

\begin{lemma}\label{satz:ZHauptidealring}
	In \( \Z \) ist jedes Ideal ein Hauptideal.
\end{lemma}

\begin{proof}
	Ist \( \mathfrak{a} = \{0\}  \), so ist \( \mathfrak{a} = (0) \). Sei also \( \mathfrak{a} \neq \{0\}  \). Dann ex. ein \( n \in \N \cap \mathfrak{a} \) (denn für \( z \in \mathfrak{a}\) ist auch \( -z  \in \mathfrak{a} \)). Setze \[
	d := \min \{z \in \N | z \in \mathfrak{a}\} 
.\]
Nun ist \( \mathfrak{a} = (d) \), denn sei \( a \in \mathfrak{a} \setminus \{0\}  \). Divison mit Rest liefert \( a = q\cdot d + r \) mit \( 0 \le r < d \), also ist \( r = a -q\cdot d \in \mathfrak{a} \). Demnach ist \( r = 0 \) wegen der Definition von \( d \).
\end{proof}

\begin{lemma}
	Ist \( K \) ein Körper, so ist jedes Ideal \( \mathfrak{a} \vartriangleleft K[X] \) ein Hauptideal
\end{lemma}
\begin{proof}
	Der Beweis ist wörtlich dasselbe wie oben mit \( d(X) :=\) ein Polynom in \(\mathfrak{a}\) von minimalem Grad.
\end{proof}

\begin{bemerkung}
	Ist \( K \) ein Körper, so hat \( K \) genau die Ideale \( \{0\} , K\).
\end{bemerkung}

\begin{defn}
	Sei \( \mathfrak{a} \) Dann heißen \( x, y \in R \) \uline{äquivalent modulo \( \mathfrak{a} \)} und man schreibt \( x \equiv y \mod \mathfrak{a} \) wenn \( x - y \in \mathfrak{a} \). Dies ist eine Äquivalenzrelation und sei \[
		\faktor{R}{\mathfrak{a}}:= \{ [x] \mid x \in R\}
	.\] die Menge der Äquivalenzklassen, also \( [x] = x + \mathfrak{a} \).
\end{defn}

\begin{satz}
	Ist \( \mathfrak{a} \vartriangleleft R  \) ein Ideal, so ist \( (\faktor{R}{\mathfrak{a}}, +, 0) \) eine abelsche Gruppe (wissen wir schon) und
	\begin{align*}
		\faktor{R}{\mathfrak{a}} \times \faktor{R}{\mathfrak{a}} &\longrightarrow \faktor{R}{\mathfrak{a}} \\
		[x], [y] &\longmapsto [xy]
	\end{align*} ist wohldefiniert und \( (\faktor{R}{\mathfrak{a}}, +, \cdot ) \) ist ein Ring mit \( 1 = [1] \).
\end{satz}
\begin{proof}
	Zur Wohldefiniertheit: Seien \( x' \in [x], y' \in  [y] \), also \( x' = x+a \) und \( y' = y+b \) für \( a, b \in \mathfrak{a} \). Dann ist \[
		[x'y'] = [(x+a)(y+b)] = [xy + \underbrace{ay + xb + ab}_{\in  \mathfrak{a}}] = [xy]
	.\] 
\end{proof}

\begin{beispiel}
	\( R = \Z \), \( \mathfrak{a}= n\Z  \leadsto \faktor{\Z}{n\Z} \) Ring mit \( [m]\cdot [k] = [m\cdot k] \).

	 Beispielaufgabe: Berechne \( 3^{17} \mod 80 \), also \( [3]^{17} \in \faktor{\Z}{80\Z} \). Es ist \( 3^4 = 81 \equiv 1 \mod 80 \). Dann ist  \[
		[3]^{17} = [3^17] = [3\cdot 3^16] = [3\cdot (3^4)^4] = [3] \cdot ([3]^4)^4 = [3]
	,\] also ist \( 3^{17} \equiv 3 \mod 80 \).
\end{beispiel}

\begin{bemerkung}
	ist \( \mathfrak{a}\vartriangleleft R \), so ist \[
		\pi: R \to  \faktor{R}{\mathfrak{a}}, x \to [x]
	\] ein Ringhomomorphismus mit  \( \operatorname{ker}\pi = \mathfrak{a} \)
\end{bemerkung}

\begin{satz}[Homomorphiesatz]
	Ist \( \varphi: R\to S \) ein Ringhomomorphismus, so ist \[
		\overline{\varphi}: \faktor{R}{\operatorname{ker} \varphi} \to  S, [x] \to \varphi(x)
	.\] ein injektiver Ringhomomorphismus (insbesondere wohldefiniert).
\end{satz}

\begin{proof}
	Folgt direkt aus Satz \ref{satz:homomorphiesatz_gruppen}.
\end{proof}

\begin{bemerkung}
	Ein Ringisomorphismus ist auch hier ein bihektiver Homomorphismus, dessen Umkehrabbildung ebenfalls ein Homomorphismus ist.
\end{bemerkung}

Betrachte das Beispiel von oben: \( L | K \) und \( \alpha \in L \) algebraisch. Dann ist
\begin{align*}
	\varphi: K[X] &\longrightarrow [\alpha]=K(\alpha) \\
	f(X) &\longmapsto f(\alpha)
.\end{align*} Dann ist \( \operatorname{ker}\varphi = (p(X)) \), \( p \) MiPo von \( \alpha \) über \( K \). Dann ist \[
	\overline{\varphi}: \faktor{K[X]}{(p(X))} \longbijects K(\alpha)
.\] ein Ringisomorphismus.

\begin{defn}
	Ein Ideal \( \mathfrak{p} \vartriangleleft R \) heißt \uline{Primideal}, wenn \( y \neq R \) und für \( x, y \in R \) mit \( xy \in \mathfrak{p} \) gilt, dass \( x \in \mathfrak{p} \) oder \( y \in  \mathfrak{p} \).
\end{defn}

\begin{bemerkung}
	\begin{itemize}
	\item In \( R \) hat man Teilbarkeit: \( x | y :\Longleftrightarrow \) es ex. \( z \in R \) mit \( y = x\cdot z \). Beachte, dass \( x | y \iff (y) \subset (x) \iff y \in (x) \)
	\item In \( R=\Z \) gilt \[
			n\Z = (n) \text{ ist Primideal } \iff n=p \text{ Primzahl oder } n = 0
	.\] 
	\end{itemize}
\end{bemerkung}

% Vorlesung vom 18. November

\begin{proof}
		"`\( \Longleftarrow \)"' Ist \( n \in \Z \) prim, so ist \( (n) \neq \Z \). Für \( x, y \in \Z \) betrachte den Fall  \[
				xy \in (n)
			.\] Das bedeutet \( n | xy \). Da  \( n \) prim ist, muss \( n | x \) oder \( n | y \) gelten, also \( x\in (n) \) oder \( y \in (n) \).\\
			Ist \( n = 0 \), so gilt  \[
				xy\in (0) = \{0\} \implies xy = 0 \overset{\in \Z}\implies x = 0 \lor y = 0 \implies x \in (0) \lor y \in (0)
			.\]

		"`\( \Longrightarrow \)"' Sei \( (n) \) ein Primideal. Ist \( n\neq 0 \) und \( n \) keine Primzahl, so existiert \( n = xy \) für  \( x,y \in \Z \) und \( x,y \neq \pm 1 \). Also ist \( xy \in (n) \) und demnach auch \( x \in (n) \lor y \in (n) \). Sei o.B.d.A \( x \in (n) \).

			Wenn \( x \in (n) \), so muss \( x = k\cdot n \) für \( k \in \N \) sein, also \( n = xy = kyn \). Demnach gilt  \[
				n \cdot (1 - ky) = 0
			.\]
	 Da wir in \( \Z \) sind, folgt \( 1 - ky = 0 \implies ky = 1 \implies y = 1 \). Widerspruch.
\end{proof}

\begin{defn}
	Ein Element \( x \in  R, x \neq 0 \) heißt \uline{Nullteiler}, wenn es ein \( y \in R \setminus \{0\}\) gibt, sodass \(xy=0\) gilt. Ein Ring \( R \) heißt \uline{nullteilerfrei} oder \uline{Integritätsbereich}, wenn er keine Nullteiler hat.
\end{defn}

\begin{defn}
	Ist \( R \) ein Ring, so ist \( R[X] := \{a_0 + a_1X + \ldots+ a_nX^n | a_j \in R\}  \) mit der üblichen Addition und Multiplikation der Polynomring über \( R \).
\end{defn}

Insbesondere: Ist \( K \) ein Körper, so ist
\begin{align*}
	\smash[b]{\underbrace{K[X_1]}_{\text{Ring}}}[X_2] &= \{a_0(X_1) + a_1(X_1)X_2 + \ldots + a_n(X_1)X_2^n | a_j(X_1) \in K[X_1]\} \\
					       &= \{(a_{00}+a_{01}X_1 + \ldots + a_{0N}X_1^N) + (a_{10}+a_{11}X_1 +\ldots + a_{1N}X^N) + \ldots + \\ & \quad\quad + (a_{n0}+a_{n1}X_1 + \ldots + a_{nN}X_1^N)X_2^n | a_{ij} \in K \}  \\
					       &= \{\sum_{i=1}^{n} \sum_{j=1}^{N} a_{ij} X_1^j X_2^i | a_{ij} \in K \} 
.\end{align*}
Sukzessive gilt dann \[
	K[x_1, \ldots, X_n] = \{ \sum_{i_1, \ldots, i_n}^{N} a_{i_1, \ldots, i_n} X_1^{i_1}\ldots X_n^{i_n}| a_{i_1, \ldots, i_n}\in K\} 
.\] 

\begin{lemma}
	Ist \( R \) nullteilerfrei, so gilt in \( R[X] \):  \[
		\operatorname{deg}(fg) = \operatorname{deg}(f) + \operatorname{deg}(g)
	.\] Insbesondere ist \( R[X] \) wieder nullteilerfrei.
\end{lemma}
\begin{proof}
	Seien \( f(X) = a_0+a_1X+\ldots+a_nX^n \) mit \( a_n \neq 0 \), so ist \( \operatorname{deg}f = n \) und \( g(X) = b_0+\ldots+b_mX^m \) mit \( b_m \neq 0 \), so ist \( \operatorname{deg}g = m \).

	Dann ist \[
		f(X)g(X) = a_nb_m X^{n+m} + (a_nb_{m-1}+a_{n-1}b_m)X^{nm-1}+ \text{ kleinere \( X \)-Polynome}
	.\] Es ist \( a_nb_m \neq  0 \), weil \( a_n \neq  0 \), \( bm \neq  0 \) und \( R \) nullteilerfrei ist. Also ist \( \operatorname{deg}(fg) = n+m \).

	Ist \( f(X)g(X) = 0 \) und \( f, g \neq  0 \), so ist \( \operatorname{deg}fg = \operatorname{deg}f + \operatorname{deg}g \ge 0 \), Widerspruch.
\end{proof}
\begin{korollar}
	Ist \( K \) ein Körper, so ist \( K[X_1, \ldots, X_n] \) nullteilerfrei.
\end{korollar}
\begin{bemerkung}
	Ist \( R \) nicht notwendig nullteilerfrei, so gilt immer noch in \( R[X] \)  \[
		\operatorname{deg}(fg) \le  \operatorname{deg}f + \operatorname{deg}g
	,\] es kann aber auch \( < \) vorkommen.
\end{bemerkung}

\begin{beispiel}
	Betrachte \( \faktor{\Z}{6\Z} \), dann sind \( [2] \) und \( [3] \) Nullteiler. Dann ist \[
		\operatorname{deg}(([2]X+[1])([3]X+[5])) = \operatorname{deg}([2][3]X^2 + [13]X + [5]) = 1 < 2 = \operatorname{deg}(f) + \operatorname{deg}(g)
	.\] 
\end{beispiel}

\begin{satz}
	Ist \( R \) ein Ring, so gilt für ein Ideal \( \mathfrak{a}\vartriangleleft R \) \[
	\faktor{R}{\mathfrak{a}} \text{ ist nullteilerfrei } \iff \mathfrak{a} \text{ ist ein Primideal}
	.\] 
\end{satz}

\begin{proof}
\begin{description}
	\item["`\( \Longrightarrow \)"'] Seien \( x, y \in R \) so, dass \( xy \in \mathfrak{a} \). Dann gilt in \( \faktor{R}{\mathfrak{a}} \) \begin{align*}
			[x][y] &= [xy] = [0] \in \faktor{R}{\mathfrak{a}} \\
			\overset{\faktor{R}{\mathfrak{a}}\text{ ntf.}}{\implies} [x] &= [0] \lor [y] = [0] \implies [x] \in \mathfrak{a} \lor [y] \in \mathfrak{a}
		.\end{align*}
	\item["`\( \Longleftarrow \)"'] Ist  \( [x][y] = 0 \in  \faktor{R}{\mathfrak{a}} \), so gilt \( xy \in \mathfrak{a} \). Da \( \mathfrak{a} \) ein Primideal ist, folgt \( x \in \mathfrak{a} \lor y \in \mathfrak{a} \), also \( [x] = [0] \lor [y] = [0] \).
\end{description}
\end{proof}

Später wollen wir \( L | K \), \( a \in L \) algebraisch. Sei \( f(X) \in K[X] \) das Minimalpolynom von \( x \) über \( K \) und
\begin{align*}
	\overline{\varphi}: \faktor{K[X]}{f(x)} &\longinjects K(\alpha) \text{ surjektiv (bijektiv)}. \\
	h(X) &\longmapsto h(\alpha)
\end{align*}
Das ist also ein Isomorphismus von Ringen, demnach ist die linke Seite ein Körper und nullteilerfrei  \( \implies (f(X)) \vartriangleleft K[X] \) ist ein Primideal.

\begin{defn}
	Ein Ideal \( \mathfrak{m}\vartriangleleft R \) heißt maximales Ideal, wenn \( \mathfrak{m}\neq R \) und wenn gilt: Ist \( \mathfrak{a}\vartriangleleft R \) mit \( \mathfrak{m} \subsetneq \mathfrak{a} \subset R \), so ist \( \mathfrak{a} = R \).
\end{defn}
Äquivalent dazu ist folgende Aussage: Ist \( \mathfrak{a}\vartriangleleft R \) mit \( \mathfrak{m}\subset \mathfrak{a}\subsetneq R  \), so ist \( \mathfrak{a} = \mathfrak{m} \).
\begin{erinnerung*}
	Es gilt \[
	\mathfrak{a} = R \iff 1 \in \mathfrak{a} \quad \text{ für } a \vartriangleleft R
	.\] 
\end{erinnerung*}
\begin{satz}
	Sei \( R \) Ring und \( \mathfrak{a}\vartriangleleft R \) ein Ideal, \( \mathfrak{a}\neq R \), so gilt \[
	\faktor{R}{\mathfrak{a}} \text{ ist ein Körper } \iff  \mathfrak{a}\text{ ist ein maximales Ideal}
	.\] 
\end{satz}
\begin{proof} \begin{description}
	\item["`\( \Longleftarrow \) "'] zz. \( (\faktor{R}{\mathfrak{a}})^\times = (\faktor{R}{\mathfrak{a}})\setminus\{0\}  \).

		Sei also \( [x] \in \faktor{R}{\mathfrak{a}}\setminus \{0\}  \), somit gilt \( x \not\in \mathfrak{a} \). Betrachte das Ideal \( \mathfrak{b} := \mathfrak{a} + Rx = \{a+rx | a \in \mathfrak{a}, r \in R\}\vartriangleleft R  \) mit \( \mathfrak{a} \subsetneq \mathfrak{b} \subset R  \). Da \( \mathfrak{b} \neq \mathfrak{a} \) gilt und \( \mathfrak{a} \) maximal ist, folgt \( \mathfrak{b} = R \ni 1 \). Daraus folgt, dass es ein \( a \in \mathfrak{a} \) und ein \( r \in R \) gibt, sodass \( 1 = a + rx \). Also ist  \[
			[1] = [a+rx] = [a] + [rx] = [r][x]
		,\] also hat \( [x] \) ein Inverses in \( \faktor{R}{\mathfrak{a}} \).

	\item["`\( \Longrightarrow \) "'] Ist \( \faktor{R}{\mathfrak{a}} \) ein Körper und sei \[
	\mathfrak{a} \subsetneq \mathfrak{b} \subset R 
	.\] eine Kette von Idealen. Zu zeigen: \( \mathfrak{b} = R \), d.h. \( 1 \in \mathfrak{b} \).

	Da \( \mathfrak{b} \supsetneq \mathfrak{a} \), ex. \( x \in \mathfrak{b}\setminus\mathfrak{a} \). Also ist \( [x] \neq  0 \in \faktor{R}{\mathfrak{a}} \). Da \( \faktor{R}{\mathfrak{a}} \) ein Körper ist, existiert ein Inverses \( [y] \in \faktor{R}{\mathfrak{a}} \), d.h. \( [x][y] = [1] \).

	Also ist  \( a := xy - 1 \in \mathfrak{a} \implies 1 = xy - a \in \mathfrak{b} \implies \mathfrak{b} = R\).
\end{description} \end{proof}

\begin{korollar}
	Ist \( R \) ein Ring, \( \mathfrak{a}\vartriangleleft R \), \( a \neq R \), so gilt \[
	\mathfrak{a}\text{ maximales Ideal} \implies \mathfrak{a} \text{ Primideal}
	.\] 
\end{korollar}
\begin{proof}
	\[
	\mathfrak{a} \text{ maximal} \implies \faktor{R}{\mathfrak{a}} \text{ Körper} \implies \faktor{R}{\mathfrak{a}}\text{ nullteilerfrei} \implies \mathfrak{a}\text{ Primideal}
	.\] 
\end{proof}
Im Allgemeinen gilt die Umkehrung nicht.\\

% Vorlesung vom 21. November

Unter Verwendung des Lemmas von Zorn gilt der folgende Satz:
\begin{satz}
	Sei \( R \) ein Ring, \( \mathfrak{a} \vartriangleleft R \), \( \mathfrak{a} \neq R \), dann ex. ein maximales Ideal \( \mathfrak{m} \vartriangleleft R \) mit \( \mathfrak{a} \subset \mathfrak{m} \).
\end{satz}
\begin{proof}
	Sei \( A:= \{ \mathfrak{b} \vartriangleleft R | \mathfrak{b} \neq  R, \mathfrak{}\subset \mathfrak{b}\}  \), und setze durch \( \mathfrak{b}\le \mathfrak{b'} :\Leftrightarrow b \subset b' \) eine reflexive, transitive und antisymmetrische Relation. Sei \( T \subset A \) eine totalgeordnete Teilmenge. Dann hat \( T \) in \( A \) eine obere Schranke, denn für \( T \) setze  \[
	\mathfrak{c} := \bigcup_{\mathfrak{b}\in T} \mathfrak{b}
	.\] Dann ist \( \mathfrak{c} \vartriangleleft R \), denn
	\begin{itemize}
		\item \( 0 \in \mathfrak{c} \), weil \( 0 \in \mathfrak{b} \) 
		\item \( a, b \in  \mathfrak{c} \implies \) es ex. \( \mathfrak{b} \in T \) mit \( a \in \mathfrak{b} \) und ein \( \mathfrak{b}' \in T \) mit \( b \in \mathfrak{b}' \). OBdA sei \( \mathfrak{b} \le \mathfrak{b}' \). Also ist \( \mathfrak{a} \in  \mathfrak{b} \subset \mathfrak{b}' \) und \( b \in  \mathfrak{b}' \), also ist \( a + b \in \mathfrak{b}' \subset c \).
		\item \( \mathfrak{a}\in  \mathfrak{c}, r \in R \implies \) es ex. ein \( \mathfrak{b} \in T \) mit \( a \in \mathfrak{b} \) und demnach auch \( ra \in \mathfrak{b} \implies ra \in \mathfrak{c} \).
	\end{itemize}

	Ferner ist \( \mathfrak{c} \neq R \), denn \( 1 \not\in  \mathfrak{b} \) für alle \(  \mathfrak{b}\in T \subset A \implies 1 \not\in \mathfrak{c} \).

	Nach dem Lemma von Zorn existiert deshalb ein maximales Element \( \mathfrak{m} \in A \), d.h. für \( \mathfrak{b} \in A \) mit \( \mathfrak{m}\le \mathfrak{b} \) gilt schon \( \mathfrak{m} = \mathfrak{b} \). Also ist \( \mathfrak{m} \) ein maximales Ideal mit \( \mathfrak{a} \subset  \mathfrak{m} \).
\end{proof}

\begin{beispiel}
	In \( \Z \) ist \( \mathfrak{a} = \Z\cdot n = (n) \) maximal genau dann, wenn \( n \) eine Primzahl ist.
\end{beispiel}
\begin{proof}
	Behauptung: \( \faktor{\Z}{n\Z} \) ist Körper \( \iff \) \( n  \) ist Primzahl, denn:
	\begin{description}
		\item["`\( \Longrightarrow \)"'] Ist \( n \) keine Primzahl, so ist \( n = \cdot b \) mit \( a, b \not\in \{ \pm 1\} = \Z^\times \). Dann ist \[
				[a]\cdot [b]=[n] = [0] \in \faktor{\Z}{n\Z}
		,\] also hat \( \faktor{\Z}{n\Z} \) Nullteiler, demnach ist \( \faktor{\Z}{n\Z} \) kein Körper.
	\item["`\( \Longleftarrow \)"'] Ist \( n = p \) eine Primzahl, so betrachte \( \faktor{\Z}{p\Z} \). Zu zeigen: Für \( [x] \neq 0 \) ex. ein multiplikatives Inverses. Ist \( [x] \neq  0 \), so also \( p \nshortmid x \), also ist  \( \operatorname{ggT}(p, x) = 1. \) Nach dem Lemma von Bézout  existieren dann \( a, b \in \Z \) mit \( ap+bx=1 \). Also gilt  \[
			[1] = [ap+bx] = [a][p]+[b][x] = [b] [x]
	.\] 
	\end{description}
\end{proof}

Zum Rechnen mit Idealen:
Für \( \mathfrak{a}, \mathfrak{b} \vartriangleleft R \) setze/beachte:
\begin{align*}
	\mathfrak{a} \cap \mathfrak{b} & \vartriangleleft R \\
	\mathfrak{a} + \mathfrak{b} &:= \{a+b \in R | a \in \mathfrak{a}, b \in \mathfrak{b}\} \vartriangleleft R  \\
	\mathfrak{a} \cdot \mathfrak{b} &:= \{\sum_{i=1}^{N} a_ib_i \in R | a_i \in \mathfrak{a}, b_i \in \mathfrak{b}, N \in \N\} \vartriangleleft R
.\end{align*}

Beachte:
\begin{itemize}
	\item \( \mathfrak{a}\cdot \mathfrak{b} \subset \mathfrak{a} \cap \mathfrak{b} \) 
	\item \( \mathfrak{a}^2 = \mathfrak{a}\cdot \mathfrak{a} = \{\sum_{i=1}^{N} a_i\cdot \tilde{a}_i | a_i, \tilde{a}_i \in \mathfrak{a}\}  \) 
	\item \( \mathfrak{a}^2 \subset \mathfrak{a} \)
\end{itemize}

\begin{defn}
	Zwei Ideale \( \mathfrak{a}, \mathfrak{b} \vartriangleleft R \) heißen \uline{teilerfremd}, wenn \( \mathfrak{a} + \mathfrak{b} = R\).
\end{defn}
\begin{bemerkung}
	In \( \Z \) gilt: \( (a), (b) \vartriangleleft\Z \) sind teilerfremd genau dann, wenn \( a, b \) teilerfremd sind, denn \begin{align*}
		(a),(b) \vartriangleleft\Z \text{ teilerfremd} &\iff (a)+(b) = R \iff (a) + (b) \ni 1 \iff \exists x,y \in Z: xa+yb = 1\\ 
							       &\iff a,b \text{ teilerfremd}
	.\end{align*}
\end{bemerkung}

\begin{satz}[Chinesischer Restsatz]
	Seien \( R \) Ring und \( \mathfrak{a}_1, \ldots, \mathfrak{a}_n \vartriangleleft R \) paarweise teilerfremd, dann ist \begin{align*}
	\faktor{R}{\bigcap_{i=1}^n \mathfrak{a}_i } &\longrightarrow \faktor{R}{\mathfrak{a}_1} \times \ldots \times \faktor{R}{\mathfrak{a}_n}  \\
	[x] &\longmapsto ([x], \ldots, [x]) 
.\end{align*} ein Isomorphismus von Ringen.
\end{satz}
\begin{proof}
Wie verstehen wir das?
\begin{itemize}
	\item Für Ringe \( R, S \) ist in kanonischer Weise \( R \times S \) ein Ring mit komponentenweiser Verknüpfung. Dabei hat \( R \times S \) Nullteiler, denn \( (1,0)\cdot (0,1) = (0,0) \) 
	\item Die Abbildungsvorschrift lautet besser \[
			x \mod \bigcap_{i=1}^n \mathfrak{a}_i \mapsto (x \mod \mathfrak{a}_1, \ldots, x \mod \mathfrak{a}_n)
	.\] 
\item Die Abbildung ist sicher ein Ringhomomorphismus.
\item Injektiv: Betrachte den Homomorphiesatz für \begin{align*}
	\varphi: R &\longrightarrow \faktor{R}{\mathfrak{a}_1} \times \ldots \times \faktor{R}{\mathfrak{a}_n} \\
	x &\longmapsto (x \mod \mathfrak{a}_1, \ldots, x \mod \mathfrak{a}_n)
.\end{align*} Dabei ist \[
	\operatorname{ker}\varphi = \{x \in R | (x \mod \mathfrak{a}_1, \ldots, x\mod \mathfrak{a}_n) = (0, \ldots, 0) \} = \bigcap_{i=1}^n \mathfrak{a}_i 
.\] Wir wissen deshalb: \[
\overline{\varphi} : \faktor{R}{\bigcap_{i=1}^n \mathfrak{a}_i } \longinjects \faktor{R}{\mathfrak{a}_1} \times \ldots \times \faktor{R}{\mathfrak{a}_n}
.\] 
\item Surjektiv: \( \overline{\varphi} \) surjektiv \( \iff \varphi  \) surjektiv \( \iff \) klassische Version gilt:
\end{itemize}
\end{proof}

\begin{satz}[Klassische Version des Chinesischen Restsatzes]
	Sei \( R \) ein Ring, \( \mathfrak{a}_1, \ldots, \mathfrak{a}_n \) paarweise teilerfremde Ideale, gegeben \( x_1, \ldots, x_n \in R \) beliebig. Dann ex. ein \( x \in R \) mit
	\begin{align*}
		x &\equiv x_1 \mod \mathfrak{a}_1 \\
		x &\equiv x_2 \mod \mathfrak{a}_2 \\
		  &\vdots \\
		x &\equiv x_n \mod \mathfrak{a}_n
	.\end{align*}
\end{satz}

\begin{satz}[Chinesischer Restsatz für \( \Z \)]
	Sind \( a_1, \ldots, a_n \in \Z \) paarweise teilerfremde Zahlen und \( x_1, \ldots, x_n \in \Z \) beliebig, so ex. ein \( x \in \Z \) mit 
	\begin{align*}
		x &\equiv x_1 \mod a_1 \\
		x &\equiv x_2 \mod a_2 \\
		  &\vdots \\
		x &\equiv x_n \mod a_n
	.\end{align*}
	Hat \( x \) eine Lösung, so sind alle Lösungen von der Form \( x + k\cdot a_1\cdot \ldots\cdot a_n) \), \( k \in \Z \).
\end{satz}

\begin{proof}
	Gegeben \( x_1, \ldots, x_n \in R \), gesucht \( x \in R \) mit \( x - x_j \in \mathfrak{a}_j \). Sei nun \( j \in \{1, \ldots, n\} \). Dann ex. für alle \( i = 1, \ldots, n \neq j \) \[
	a_{ij} \in \mathfrak{a}_i \text{ und } b_{ij} \in \mathfrak{a}_j
	.\] mit \( a_{ij}+b_{ij} = 1 \).
	Setze \( s_j:= \prod_{i = 1, \ldots, n; i \neq j} a_{ij} \in R   \). Es gilt:
	\begin{enumerate}[{1)}]
		\item \( s_j \in \mathfrak{a}_i \) für alle \( i \neq j \).
		\item \( s_j = \prod_{i = 1,\ldots,n; i \neq j}(1-b_{ij}) = 1 - (b_{ij}+\ldots + b_{nj}) + \sum \ldots \equiv 1 \mod \mathfrak{a}_j \)
	\end{enumerate}

	Also gilt \( s_j \equiv 0 \mod a_{i} \) für \( i \neq j \) und \( s_j \equiv 1 \mod \mathfrak{a}_j \).

	Setze nun  \[
	x := x_1s_1 + x_2s_2 + \ldots + x_n s_n \in R
	.\] Das ist eine Lösung.
\end{proof}

\begin{beispiel}
	\[
		\faktor{\Z}{6\Z} \stackrel{\cong}{\longrightarrow} \faktor{\Z}{2\Z} \times \faktor{\Z}{3\Z}
	.\] Aber \[
	\faktor{\Z}{4\Z} \not\cong \faktor{\Z}{2\Z}\times \faktor{\Z}{2\Z}
	,\] denn \( \faktor{\Z}{4\Z} \) hat ein Element der Ordnung 4, aber \( \faktor{\Z}{2\Z} \times \faktor{\Z}{2\Z} \) hat das nicht.
\end{beispiel}

