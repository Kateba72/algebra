\chapter{Teilbarkeit in Integritätsbereichen und faktorielle Ringe}

Sei \( R \) in diesem Kapitel nullteilerfrei.

\begin{defn}
	Für \( a, b \in R \) sagen wir \( a \) und \( b \) sind \uline{assoziiert} zueinander, wenn \( a | b \) und \( b | a \). Äquivalent dazu ist: Es existiert ein \( u \in R^\times \) mit \( a = b\cdot u \).
\end{defn}

\begin{proof}
	Ist \( a = bu \) mit \( u \in R^\times \), so gilt \( b | a \), aber auch  \( a\cdot u^{-1} = b \implies a | b \), also sind \( a \) und \( b \) assoziiert.

	Umgekehrt: Gilt  \( b|a \), also \( a = b\cdot c \) für ein \( c \in R \) und \( a | b \), also \( b = a\cdot \tilde{c} \) für ein \( \tilde{c} \in R \), so folgt \[
		a = b\cdot c = a\tilde{c}\cdot c \implies a - a\tilde{c}c = 0 \implies a(1-\tilde{c}c) = 0
	.\] Also gilt \( a = 0 \) oder \( \tilde{c}c = 1 \).

	Ist \( a = 0 \), so ist auch \( b = a\cdot \tilde{c} = 0 \) und es folgt \( a = b\cdot 1 \) sowie \( b = a\cdot 1 \).

	Ist \( a \neq 0 \), so ist \( \tilde{c}c = 1 \implies c \in R^\times \).
\end{proof}

\begin{frage*}
	Was sollen "`Primzahlen"' in einem Integritätsbereich \( R \) sein?
\end{frage*}
Man hat zwei Möglichkeiten: Die erste Möglichkeit ist, zu sagen, \( p\in \Z \) ist prim, wenn es keine positiven Teiler außer \( 1 \) und \( p \) hat, was genau dann der Fall ist, wenn für \( p = a\cdot b \) mit \( a, b \in \Z \) entweder \( a \in \{\pm 1\}  \) oder \( b \in \{\pm 1\} = \Z^\times  \) gilt.

Die zweite Möglichkeit ist, \( p \in Z \) als prim zu definieren, wenn für \( x,y \in \Z \) mit \( p | xy \) die Aussage \( p|x \) oder \( p | y \) gelten muss.

\begin{defn}
	Sei \( R \) ein Integritätsbereich.
	\begin{enumerate}[{1)}]
		\item Ein Element \( \pi \in R \) heißt \uline{irreduzibel}, wenn \( \pi \not\in  R^\times \) und wenn für \( a, b \in R \) mit \( \pi=ab \) eine der Aussagen \( a \in R^\times \) oder \( b \in R^\times \) gelten muss.
		\item Ein Element \( \pi \in R \) heißt \uline{Primelement}, wenn \( \pi \not\in  R^\times \) ist und wenn für \( x, y \in R \) \( \pi | xy  \implies \pi | x \) oder \( \pi | y \).
	\end{enumerate}
\end{defn}

\begin{bemerkung}
	\( \pi \in R \) ist genau dann ein Primelement, wenn \( (\pi) \vartriangleleft R \) ein Primideal ist.
\end{bemerkung}

\begin{lemma}
	Für \( \pi \in R \) gilt \[
	\pi \text{ Primelement } \implies \pi \text{ irreduzibel}
	.\] 
\end{lemma}

\begin{proof}
	Sei \( \pi = ab \) mit \( a,b \in R \), so folgt \( \pi | \pi = ab \implies \pi | a \) oder \( \pi | b \), sagen wir \( \pi | a \). Also existiert ein \( c \in R \) mit \( a = \pi c \), somit gilt \( \pi = ab = \pi \cdot c b \implies \pi(1-bc) = 0 \). Da der Ring nullteilerfrei ist, folgt \( 1 = c\cdot b \), also ist \( b \in R^\times \).
\end{proof}

\begin{lemma}
	Ist \( R \) ein Hauptidealbereich und \( \pi \in R \setminus R^\times \), dann gilt \[
		\pi \text{ irreduzibel } \iff (\pi) \vartriangleleft R \text{ maximales Ideal}
	.\] 
\end{lemma}
\begin{proof}
	\begin{description}
		\item["`\( \Rightarrow \)"'] Sei  \( (\pi) \subsetneq \mathfrak{a} \vartriangleleft R \). Es ist \( \mathfrak{a} = (a) \) für ein \( a \in R \), da \( R \) ein Hauptidealring ist; wegen \( (\pi) \subsetneq (a) \) ex. ein \( b \in R \) mit \( \pi = ab \).
			Da \( (\pi) \neq  (a) \), ist \( b \not\in  R^\times \), denn \( a \not\in (\pi) \). Da \( \pi \) irreduzibel ist, muss \( a \in R^\times \) gelten, also ist \( \mathfrak{a} = (a) = R \).
		\item["`\( \Leftarrow \)"'] Sei  \( \pi = xy \) mit \( x,y \in R \). Betrachte dann ein Ideal \[
				(\pi) \subset (x) \subset R
		.\]
		Ist \( x \in R^\times \), so sind wir fertig. Betrachte also den Fall \( x \not\in R^\times \), dann ist \( (x) \subsetneq R \). Somit ist, weil  \( (\pi) \) ein maximales Ideal ist, \( (\pi) = (x) \), insbesondere \( x \in \pi \). Also existiert ein \( z \in R \) mit \( x = \pi z \). Dann ist \( x = xyz \implies x(1-yz) = 0 \implies 1-yz = 0 \implies y \in R^\times \).
	\end{description}
\end{proof}

\begin{defn}
	Ein Integritätsbereich \( R \) heißt \uline{faktorieller Ring} (engl. UFD: ``unique factorization domain''), wenn gilt: Jedes \( a \in R \setminus (\{0\} \cup R^\times) \) hat eine eindeutige \( (^*) \) Zerlegung \[
	a = \pi_1 \cdot \ldots \cdot \pi_s \quad \text{ mit } \pi_j \in R \text{ irreduzibel}
	.\] 
	\( (^*) \) zur Eindeutigkeit: Sind \( a = \pi_1\cdot \ldots \cdot  \pi_s = q_1\cdot \ldots\cdot q_r \) zwei Zerlegungen in irreduzible Elemente, so gilt \( r = s \) und zu jedem  \( j \in \{1, \ldots, r\}  \) ex. \( \varepsilon_j \in R^\times \) \( \sigma(q) = \varepsilon_j \pi_j \) mit einer Permutation \( \sigma \in S(r) \). 
\end{defn}

\begin{satz}
	Der Integritätsbereich \( R \) ist schon dann faktoriell, wenn jedes \( a \in R \setminus(\{0\} \cup R^\times) \) eine Zerlegung \( a = \pi_1\cdot \ldots \cdot  \pi_s \) mit Primelementen \( \pi_j \in R \) hat.
\end{satz}

\begin{proof}
	Zu zeigen: Ist \( a = \pi_1\cdot \ldots\cdot \pi_r = q_1\cdot \ldots\cdot q_s \) mit Primelementen \( \pi_j \) und irreduziblen \( q_j \), so ist  \( r = s \) und nach eventuellem Umnummerieren ist  \( \pi_j \sim q_j \) (assoziiert).

	\uline{Dazu}: Da \( \pi_1 \) Primelement und \( \pi_1 | a = q_1\cdot \ldots \cdot q_s \implies \pi_1 | q_j \) für ein \( j \). OBdA sei \( j = 1  \implies \pi_1 | q_1\).
	Also gilt \( q_1 = \pi_1\cdot r_1 \) für ein \( a \in R \) und da \( q_1 \) irreduzibel und \( \pi_1 \not\in  R^\times \), folgt \( r_1 \in R^\times \), also \( q_1\sim \pi_1 \). Also folgt \( \pi_1 \cdot  \ldots \cdot  \pi_r = r_1\cdot \pi_1 \cdot q_2\cdot \ldots\cdot q_s \implies \pi_2\cdot \ldots\cdot \pi_r = (\underbrace{r_1q_2}_{\text{irred.}}) q_3\cdot \ldots\cdot q_s\). Durch sukzessives Ausführen dieses Schrittes folgt die Aussage.
\end{proof}

\begin{lemma}
	In einem faktoriellen Ring \( R \) gilt \[
	q \text{ Primelement } \iff q \text{ irreduzibel}
	.\] 
\end{lemma}
\begin{proof}
	\begin{description}
		\item["`\( \Rightarrow \)"'] gilt in jedem Integritätsbereich.
		\item["`\(\Leftarrow \)"'] Sei \( q \) irreduzibel und \( q | ab \), also es existiert ein  \( x \in R \) mit \( qx = ab \). Ist  \( a = p_1\cdot \ldots\cdot p_r \), \( b = p_1'\cdot \ldots\cdot p_s' \) eine Zerlegung in irreduzible Elemente, \( x = s_1\cdot \ldots\cdot s_t \) ebenso, so ist \[
		a\cdot s_1\cdot \ldots\cdot s_t = p_1\cdot \ldots\cdot p_r \cdot  p_1'\cdot \ldots \cdot p_s
		,\] wobei beide Seiten ein Produkt aus irreduziblen Faktoren ist. Da diese eindeutig ist, gilt \( q \sim p_j \) oder \( q \sim p_k'  \implies q|a\) oder \( q|b \).
	\end{description}
\end{proof}

\begin{satz}
	Jeder nullteilerfreie Hauptidealring ist faktoriell.
\end{satz}
\begin{proof}
	Sei \( a \in R \setminus (\{0\} \cup R^\times \). Dann hat \( a \) einen irreduziblen Teiler; angenommen nicht:

	\( a \) ist nicht irreduzibel, also \( a = x_1\cdot a_1 \) mit  \( x_1, a_1 \in R\setminus R^\times \). \( x_1 \) ist nicht irreduzibel, also \( x_1 = x_2 \cdot a_2 \) mit \( x_2, a_2 \in R \setminus R^\times \), sukzessive so weiter. Wir erhalten eine Kette von Idealen\[
		(x_1) \subsetneq (x_2) \subsetneq (x_3) \subsetneq \ldots
	,\] die nicht stationär wird. Da \( R \) ein Hauptidealring ist, insbesondere also nothersch, ist das ein Widerspruch.

	Dann hat \( a \) aber auch einen Primteiler: Ist \( q \in R \) irreduzibel, so ist \( (q) \vartriangleleft R \) maximal, also ist \( (q) \vartriangleleft R \) ein Primideal, also ist \( q  \) ein Primelement.

	Damit hat \( a  \) eine Zerlegung in Primelemente, denn \( a = q\cdot b \) mit \( q \) Primelement. Ist \( b \) eine Einheit, sind wir fertig, andernfalls wissen wir \( b = q_2b_2 \) mit \( q_2 \) Primelement wie oben. Sukzessive so weiter erhalten wir \[
	a = qq_2q_3\cdot \ldots\cdot q_rb_r \text{ mit }b_r \in R
.\] mit \( (b) \subsetneq (b_2) \subsetneq (b_3) \subsetneq \ldots \). Weil \( R \) noethersch ist, bricht dieser Prozess ab, d.h. es existiert ein \( r \in \N \) mit \( b_r \in R^{\times } \).
\end{proof}

\begin{beispiel}
	Der Ring der ganzen Zahlen in reell-quadratischen Zahlkörpern: \[
		\Q(\sqrt{d} ), \; d \in  \N\setminus \{1\}, \text{ quadratfrei}
	.\] (d.h. ist \( p | d \) Primteiler, so ist \( p^2 \nmid d \)). Dann ist \( \Q(\sqrt{d}) | \Q \) mit Körpergrad \( 2 \). 
	Betrachte den Ring:
	\begin{itemize}
		\item für \( d \equiv 1 \mod 4 \implies \Z[\omega]=\{a+b\omega | a, b \in \Z\}  \) mit \( \omega = \frac{1+\sqrt{d} }{2} \) \\
			Das Minimalpolynom von \( \omega \) über \( \Q \): \( \omega = \frac{1+\sqrt{d} }{2} \implies 2w - 1 = \sqrt{d} \implies 4\omega^2-4\omega + 1 - d = 0  \). Also ist das Minimalpolynom von \( \omega \) über \( \Q \) \[
				p(X) = X^2-X - \frac{d-1}{4} \in \Z[X]
			.\] Insbesondere ist \( \omega^2 = \omega + \frac{d-1}{4} \in \{a+b\omega| a,b \in \Z\}  \)
		\item für \( d \equiv 2,3 \mod 4 \implies \Z[\sqrt{d}] = \{a+b\sqrt{d}|a,b \in \Z \}   \).
	\end{itemize}
\end{beispiel}

Betrachte folgende Norm-Funktion:
\begin{align*}
	N:= N_{\Q(\sqrt{d})|\Q} \Q(\sqrt{d} ) &\longrightarrow \Q \\
	x+y\sqrt{d} &\longmapsto (x+y\sqrt{d})(x-y\sqrt{d}) = x^2-dy^2\\
N _{| \Z[\omega]}: \Z[\omega] & \longrightarrow \Z \text{ wenn } d \equiv 1 \mod 4\\
N _{| \Z[\sqrt{d} ]}: \Z[\sqrt{d} ] & \longrightarrow \Z \text{ wenn } d \equiv 2,3 \mod 4
.\end{align*} 
Im Fall \( d \equiv 1 \mod 4\): \( a + b\omega = (a+\frac{b}{2}) + \frac{b}{2}\sqrt{d} \) und damit \( N(a+b\omega) = (a+\frac{b}{2})^2-d \frac{b^2}{4} = a ^2 + ab + b^2 \frac{1-d}{4}\).

\( N \) ist multiplikativ, d.h. \( N(\xi\cdot \eta) = N(\xi)\cdot N(\eta) \) 

\begin{lemma}
	Für \( \xi \in \Z[\omega] \) (bzw. \( \Z[\sqrt{d}] \)) gilt \[
		\xi \text{ ist Einheit } \iff N( \xi) = \pm 1 \in \Z^\times
	.\] 
\end{lemma}

\uline{Explizites Beispiel}: \( d = 10 \equiv 2 \mod 4 \). In \( \Z[\sqrt{10}] \) gilt \[
\sqrt{10} \cdot  \sqrt{10}  = 10 =  2\cdot 5
.\]sind zwei verschiedene irreduzible Zerlegungen.
\( 2 \in \Z[\sqrt{10}] \) ist irreduzibel, denn \(2=xy \implies 4 = N(2) = N(x)N(y) \in \Z\), d.h. \( N(x) = \pm 1 \) (d.h. \( x \) Einheit) oder \( N(y) = \pm 1 \) (d.h. \( y \) Einheit) oder \( N(x) = \pm 2 = N(y) \). Das kann nicht sein, wie eine Rechnung ergibt. Also ist \( \Z[\sqrt{10}] \) nicht faktoriell. \( \Z[\sqrt{22} ] \) ist hingegen faktoriell.
