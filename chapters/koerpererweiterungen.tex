\chapter{Algebraische Körpererweiterungen}
\begin{defn}
	Sei \( L \) ein Körper. Eine Teilmenge  \(  K \subset L \) heißt  \uline{Teilkörper}, wenn 
	\begin{itemize}
		\item \( a, b \in K \implies a+b,\, a \cdot b \in K \)
		\item vermöge \( K \times K \to  K, \; (a,b) \mapsto  a+b \) und \( K \times  K \to K,\; (a,b) \mapsto  a\cdot b \) \( K \) wieder ein Körper ist (mit \( 0,1 \in  K \subset  L \)).
	\end{itemize}
	Dann heißt \(  K \subset  L \) eine \uline{Körpererweiterung} und man schreibt \( L \vert K \) (sprich: \( L \) über \( K \)).
\end{defn}

\begin{beispiel}
	\( L := \Q[\sqrt{2} ] :=\{a+b\sqrt{2} \in \R | a + b \in \Q \}  \) ist ein Teilkörper von \( \R \), Beweis Übung. Also ist \( \R | L \) eine Körpererweiterung, aber auch \( L | \Q \).
\end{beispiel}

\textbf{Motivation}: Die Gleichung \[
X^2 - 7X + 1 = 0
.\] hat die Lösung \( \alpha = \frac{7 \pm \sqrt{49-4} }{2} = \frac{7\pm 3 \sqrt{5} }{2} \in \C \).
Nun ist \( \C | \Q(a) := \text{ kleinster Teilkörper } \Q \subset L \subset \C \text{ mit } a\in L\) und \( \Q(a) | Q \), aber auch \( \C | \Q[\sqrt{5}]  \) und \( \Q[\sqrt{5}] | \Q  \). Es ergibt sich mithilfe der Mitternachtsformel, dass \( \Q[\sqrt{5}] = \Q(a) \) ist. Ziel ist es, aus solchen Informationen Wissen über \( \alpha \) zu gewinnen.

\begin{lemma}
	Ist \( L | K \) eine Körpererweiterung, so hat man die Einschränkung der Multiplikation auf \( L \) : 
	\begin{align*}
		 K \times L &\longrightarrow L \\
		\lambda, v &\longmapsto \lambda\cdot v.
	.\end{align*}
	Diese macht \( L \) zu einem \( K \)-Vektorraum.
\end{lemma}

\begin{defn}
	Eine Körpererweiterung \( L|K \) heißt \uline{endlich}, wenn \( \operatorname{dim}_K L < \infty \). Dann heißt \( [L:K] := \operatorname{dim}_K L \) der \uline{Körpergrad von \( L \) über \( K \)}.
\end{defn}
\begin{beispiel}
	\( \Q[\sqrt{2} ] \) hat die \( \Q \)-Basis \( (1, \sqrt{2}  \). Also ist \( [\Q[\sqrt{2}]: \Q] = 2 \).
\end{beispiel}

\begin{defn}
	Sind \( K \subset M \subset L \) Körper, so heißt \( M \) \uline{Zwischenkörper von \( L | K \)}.
\end{defn}
\begin{defn}
	Ist \( L | K \) gegeben und \( S \subset L \) eine beliebige Teilmenge, so schreibe \[
		K(S) := \bigcap \{M | M \text{ Zwischenkörper } K \subset M \subset L\}  
	.\] für den kleinsten Zwischenkörper \( K(S) \) mit \( S \subset K(S) \). Insbesondere schreibe \( K(\alpha) := K(\{\alpha\})  \) für ein \( \alpha \in L \).
\end{defn}
\begin{bemerkung}
	Es gilt \( K(\alpha) = \left\{\frac{f(\alpha)}{g(\alpha)}\in L \middle\vert f,g \in  L[X], g(\alpha) \neq  0\right\}  \).
\end{bemerkung}

Betrachte den Polynomring über \( K \) \[
	K[X] = \{f(X) = a_nX^{n}+ \ldots + a_1X + a_0 | a_i \in K\}
.\] Damit hat man für ein \( \alpha \in L \)\[
K[\alpha] = \{f(\alpha) = a_n\alpha^{n} + \ldots + a_1\alpha + a_0 |a_i \in K\} \subset L
.\] 
\begin{bemerkung}
	\( K[\alpha]  \) ist abgeschlossen unter Addition und Multiplikation mit \( \lambda \in K \). Also ist \( K[\alpha] \subset L \) ein \( K \)-Untervektorraum.

	Für \( f(\alpha), g(\alpha) \in K[\alpha] \) ist \( f(\alpha)\cdot g(\alpha) \in K[\alpha] \).
	\( K[\alpha]\) ist aber im Allgemeinen kein Körper.
\end{bemerkung}

\begin{defn}
	Für \( L | K \) nennt man \( \alpha \in  L \) \uline{algebraisch über \( K \)}, wenn es ein Polynom \( f(X) \in  K[X] \) gibt mit \( f(X) \neq 0 \) und \( f(\alpha) = 0 \). Sonst heißt \( \alpha \) \uline{transzendent}.
\end{defn}

\begin{defn}
	Ist \( \alpha \in L|K \) algebraisch, so heißt das Polynom \( f(X) \in K[X] \setminus \{0\}  \) mit kleinstem Grad unter allen \( g(X) \) mit \( g(\alpha) = 0 \) und führendem Koeffizienten \( 1 \) (d.h. \( f \) ist \uline{normiert}) das \uline{Minimalpolynom von \( \alpha \) über \( K \)}.
\end{defn}

\begin{beispiel}
	Betrachte \( L = \C | K = \Q \) und \( \alpha = \sqrt[3]{2} \in L\). \( \alpha \) ist algebraisch, denn \( \alpha^3 - 2 = 0 \), also ist \( f(X) = X^3-2 \) ein solches \( f \) mit \( f(\alpha) = 0 \).

	\( \Q[\sqrt[3]{2}]\) ist ein Körper, wenn für \( g(\alpha) \in  \Q[\sqrt[3]{2}]\setminus \{0\}\) ein \( h(\alpha) \in  \Q[\sqrt[3]{2}]\) mit \( g(\alpha)\cdot h(\alpha) = 1 \) existiert.

	\begin{frage*}
		Was ist \( \operatorname{dim}_\Q \Q[\alpha:=\sqrt[3]{2}]\)?\\
	\end{frage*} 
	\textbf{Dazu}: Wenn \( f(X) := X^3-2 \) das Minimalpolynom von \( \alpha \) ist, dann ist \( 1, \alpha, \alpha^2 \) eine Basis, denn:
	\begin{itemize}
		\item linear unabhängig: Angenommen, \( \lambda_0 1 + \lambda_1\alpha + \lambda_2\alpha^2 = 0 \). Da \( \lambda_0 + \lambda_1X + \lambda_2X^2 \) Grad \( 2 < 3 = \operatorname{deg} \text{MiPo } \) hat, ist \( \lambda_0 = \lambda_1 = \lambda_2 = 0 \).
		\item Erzeugendensystem: Ist \( g(\alpha) \in  \Q[\alpha] \), so betrachte Division mit Rest \[
				g(X) = q(X)\cdot f(X) + r(X) \quad \text{ mit } r = 0 \text{ oder }\operatorname{deg}r < \operatorname{deg}f
			.\] Also ist \( g(\alpha) = q(\alpha)f(\alpha) + r(\alpha) = r(\alpha) \).
	\end{itemize}
\end{beispiel}

\begin{frage*}
	Für \( f(\alpha) \in  K[\alpha]\setminus\{0\}\): Ist \( f(\alpha)^{-1} \in  K[\alpha] \)?
\end{frage*}

\begin{bemerkung}[{Besondere Eigenschaften von \( K[X] \)}]
	Man hat den Begriff \( \operatorname{deg}f := \max \{n \in \N_0 | a_n \neq  0\} \) (wenn \( f(X) \neq 0 \)) bzw. \( \operatorname{deg}0 := - \infty \).
	Dabei ist \( \operatorname{deg}(f\cdot g) = \operatorname{deg}f+ \operatorname{deg}g \) für \( f, g \in K[X] \)

	Man hat Division mit Rest: Sing \( f, g \in  K[X]\setminus \{0\}  \), so existieren eindeutige \( q, r \in  K[X] \) mit \[
	f=q\cdot g+r \text{ mit } 0 \le  \operatorname{deg}r < \operatorname{deg}g \text{ oder } r=0
	.\] 
\end{bemerkung}

\begin{lemma}
	Ist \( p(X) \in K[X] \) das MiPo von \( \alpha \) über K und \( f(X) \in K[X] \) mit \( f(\alpha) = 0 \), so existiert ein \( q(X) \in K[X] \) mit \( f(X) = p(X)\cdot q(X) \), d.h. \( p(X) | f(X) \).
\end{lemma}

\begin{proof}
	Nutze Division mit Rest, d.h. \( f(X) = q(X)p(X)+r(X) \) mit  \( r = 0 \) oder \( 0 \le \operatorname{deg} r \le  \operatorname{deg} p \). Durch Einsetzen von \( \alpha \) ergibt sich \[
		0 = f(\alpha) = q(\alpha)p(\alpha) + r(\alpha) = r(\alpha)
	.\] \( \implies r = 0 \).
\end{proof}

\begin{defn}
	Ein Polynom \( p(X) \in  K[X] \) heißt \uline{irreduzibel}, falls \( \operatorname{deg} p \ge 1 \) und wenn für jedes \( f(X), g(X) \in K[X] \) mit \( p(X)=f(X)\cdot g(X) \) entweder \( \operatorname{deg} f = 0 \) oder \( \operatorname{deg} g = 0 \) gilt.
\end{defn}

\begin{bemerkung}
	\( \operatorname{deg}f = 0 \iff f = a_0 \in  K\setminus \{0\}  \).
\end{bemerkung}

\begin{lemma}
	Ist \( p(X) \in  K[X] \) normiert, so gilt:
	\( p \) ist das MiPo von \( \alpha \) über \( K \) \( \iff \) \( p(\alpha) = 0 \) und \( p \) ist irreduzibel.
\end{lemma}
\begin{proof}
	"\( \Leftarrow \)": Ist \( p(\alpha) = 0 \) und \( p \) irreduzibel, so schreibe \( \mu(X) \in K[X] \) für das Minimalpolynom von \( \alpha \) über \( K \). Nach obigem Lemma gilt \( \mu(X)|p(X) \), also existiert \( q \in K[X] \) mit \[
		p(X) = q(X)\cdot \mu(X)
	.\]
	Da \( p \) irreduzibel ist und \( \operatorname{deg}\mu \ge 1 \), folgt \( \operatorname{deg}q = 0 \) \( \implies q = a_0 \in K\setminus \{0\} \) und \( p(X) = a_0\cdot \mu(X) \). Da beide normiert sind, gilt \( a_0=1 \) und damit \( p=\mu \).

	"\( \Rightarrow \)": Sei \( p \) das Minimalpolynom. Angenommen, \( p \) ist nicht irreduzibel. Dann existieren \( f, g \in  K[X] \) mit \( \operatorname{deg}f \ge  1, \operatorname{deg}g \ge 1 \) und \( p=f\cdot g \in K[X] \).
	Durch Einsetzen von \( \alpha \) ergibt sich: \[
		0 = p(\alpha) = f(\alpha)g(\alpha) \in L \text{ Körper}
	.\] \( \implies f(\alpha) = 0 \) oder \( g(\alpha) = 0 \), aber \( \operatorname{deg}f < \operatorname{deg}p \) und \( \operatorname{deg}g < \operatorname{deg}p \), Widerspruch zu \( p \) MiPo.
\end{proof}

Eine weitere Folgerung aus der Division mit Rest:
\begin{bemerkung}
	Ist \( f(X) \in  K[X] \) und \( f(\alpha) = 0 \) für \( \alpha \in  L \), \( L | K \), dann betrachte \( f(X) \in L[X] \). Es gilt \[
		f(X) = (X-\alpha) \cdot  h(X) \text{ für ein }h(X) \in  L[X]
	.\] mit \( \operatorname{deg}h = \operatorname{deg}f - 1 \).
\end{bemerkung}
\begin{beispiel}
	\begin{align*}
		X^2-2 &= (X-\sqrt{2})(X+\sqrt{2})\\
		\in \Q[X] &\quad \in \Q(\sqrt{2})[X]
	.\end{align*}
\end{beispiel}

Für die Frage nach Algebraizität von \( \alpha \) gibt es folgenden
\begin{satz}
	Sei \( L|K \) eine Körpererweiterung, \( \alpha \in L \). Dann sind folgende Aussagen äquivalent.
	\begin{enumerate}[{i)}]
		\item \( \alpha \) ist algebraisch über \( K \).
		\item \( K[\alpha] \) ist ein Körper
		\item \( K[\alpha] = K(\alpha) \)
		\item \( [K(\alpha):K]< \infty \).
	\end{enumerate}
\end{satz}
\begin{proof}
	\begin{description}
		\item[(i) \(\Rightarrow\) (ii)] Dann hat \( \alpha \) ein MiPo \( p(X) \in  K[X] \) über \( K \). Sei \( \operatorname{deg}p =: n \). Ist dann \( f(\alpha) \in K[\alpha] \), so ist nach Division mit Rest \[
		f(X) = q(X) p(X) + r(X) \text{ mit } r=0 \text{ oder } \operatorname{deg}r < \operatorname{deg}p = n.
	.\] \( \implies f(\alpha) = r(\alpha) = r_0+ r_1\alpha ^{1}+\ldots r_{n-1}\alpha ^{n-1} \). Also sind \( 1, \alpha, \ldots, \alpha ^{n-1} \in K[\alpha] \) ein Erzeugendensystem von \( K[\alpha] \) als \( K \)-Vektorraum. Es ist sogar eine Basis, weil \( \operatorname{deg}p = n \).
	Zu zeigen: Ist \( f(\alpha) \neq  0 \in  K[\alpha] \), so ex. ein Inverses \( g(\alpha) \in  K[\alpha]  \) mit \( f(\alpha)g(\alpha) = 1 \). Dazu betrachte die Multiplikation mit \( f(\alpha) \):
	\begin{align*}
		\psi: K[\alpha] &\longrightarrow K[\alpha] \\
		h(\alpha) &\longmapsto f(\alpha)h(\alpha)
	.\end{align*} Diese ist \( K \)-linear, \( K[\alpha] \) ist endlichdimensional und \( \psi \) ist injektiv, also ist wegen des Rangsatzes \( \psi \) dann auch surjektiv, insbesondere hat \( 1 \in K[\alpha] \) ein Urbild.

\item[(ii) \( \Leftrightarrow \) (iii)] Ist \( K \subset M \subset  L \) ein Zwischenkörper mit \( \alpha \in M \), dann muss \( K[\alpha] \in  M \). Ist \( K[\alpha] \) ein Körper, so ist \( K[\alpha] \) also schon der kleinste Körper mit \( \alpha \in K[\alpha] \). Ist \( K[\alpha] = K(\alpha) \), so ist \( K[\alpha] \) schon ein Körper.

\item[(iii) \( \Rightarrow \) (iv)] Ist \( K[\alpha] = K(\alpha) \), so ist \( K[\alpha] \) Körper und damit hat insbesondere \( \alpha \in  K[\alpha] \setminus \{0\} \) ein Inverses. Also existier \( h(X) \in K[X] \) mit \( \alpha \cdot h(\alpha) = 1 \). Also ist \( f(X) = X \cdot h(X) - 1  \in  K[X] \setminus \{0\} \) erfüllt \( f(\alpha) = 0 \implies \alpha \) algebraisch. Nach obiger Erkenntnis im Beweis: \[
		[K(\alpha):K] = \operatorname{dim}_{K} K(\alpha) = \operatorname{dim}_K K[\alpha] = \operatorname{deg} \text{ MiPo } < \infty
	.\]

\item[(iv) \( \Rightarrow \) (i)] Ist \( [K[\alpha]:K]<\infty \), sagen wir \( n:= [K(\alpha):K] \). Dann sind \( 1, \alpha, \alpha^2, \ldots, \alpha ^{n} \) libear abhängig. Es existieren also \( \lambda_0,\ldots,\lambda_n \in K \), nicht alle gleich 0, mit  \( \lambda_0 \cdot 1 + \lambda_1\alpha + \ldots + \lambda_n \alpha ^{n} = 0 \). Also erfüllt \[
		f(X) := \lambda_0 + \lambda_1X + \ldots \lambda_n X^{n}\in K[X]\setminus \{0\} 
	.\] \( f(\alpha) = 0 \). Also ist \( \alpha \) algebraisch.

		
	\end{description}
\end{proof}
\begin{lemma}[Erkenntnis aus dem Beweis]\label{lemma:KoerpergradGleichMipograd}
	Ist \( \alpha \in L \) algebraisch über \( K \) und \( f(X) \in  K[X] \) das Minimalpolynom von \( \alpha \) über \( K \), so ist \[
		[K[\alpha] : K] = \operatorname{deg} f
	.\] 
\end{lemma}

\begin{defn}
	\( L | K \) heißt \uline{algebraisch über \( K \)}, wenn jedes \( \alpha \in L \) algebraisch über \( K \) ist.
\end{defn}

\begin{korollar}
	Ist \( L | K \) endlich, so ist \( L | K \) algebraisch.
\end{korollar}
\begin{proof}
	Ist \( \alpha \in L \), so ist nach Lemma \ref{lemma:KoerpergradGleichMipograd} \( [K(\alpha):K] < \infty \), also ist \( \alpha \) algebraisch über \( K \).
\end{proof}

\begin{satz}[Gradformel]
	Sind \( L | M | K \) Körpererweiterungen, so gilt: \[
		[L:K] = [L:M] \cdot [M:K]
	.\] 
\end{satz}
\begin{proof}
	Angenommen, \( [L:M] < \infty \) und \( [M:K] < \infty \). Wähle nun eine Basis \( m_1, \ldots, m_s \) in \( M \) als \( K \)-VR und eine Basis \( l_1, \ldots, l_r \) von \( L \) als \( M \)-VR.\\
	\uline{Behauptung:} \( m_1l_1, m_1l_2, \ldots, m_1l_r, m_2l_1, \ldots, m_2l_r, \ldots,m_sl_1, \ldots, m_sl_r \) ist eine \( K \)-Basis von \( L \).
	\begin{description}
		\item [Erzeugendensystem] Ist \( \alpha \in L \), so hat man eine Darstellung von \( \alpha \) als \( M \)-Linearkombination der \( l_1, \ldots, l_r \in L \)\[
		\alpha = \mu_1l_1 + \ldots + \mu_rl_r \text{ mit }\mu_j \in M
		.\] Jedes \( \mu_j \) hat eine Darstellung in der \( K \)-Basis \( m_1,\ldots,m_s \) von \( M \) \[
		\mu_j = \kappa_{j1}m_1+\ldots+\kappa_{js}m_s \text{ mit }\kappa_{ji}\in K
		.\] Also ist \[
		\alpha = \mu_1l_1+\ldots + \mu_rl_r = \left( \sum_{i=1}^{s} \kappa_{1i}mi \right) l_1 + \ldots + \left( \sum_{i=1}^{s} \kappa_{ri}m_i \right) l_r = \sum_{j=1}^{r}\sum_{i=1}^{s} \kappa_{ji}(m_il_j)
	\] eine Linearkombination der \( m_il_j \), \( i=1,\ldots,s \), j=\( 1,\ldots,r \).
\item [linear unabhängig] Sei \( \sum_{i= 1}^{s} \sum_{j=1}^{r} \kappa_{ji}m_il_j = 0 \) mit \( \kappa_{ji}\in K \). Da \( l_1, \ldots, l_r \) eine \( M \)-Basis ist, insbesondere also linear unabhängig über M, gilt für alle \( j \in 1,\ldots,r \), dass \( \sum_{i=1}^{s} \kappa_{ji}m_i = 0 \). Da die\( m_1, \ldots, m_s \) linear unabhängig über \( K \) sind, ist dann \( \kappa_{ji} = 0 \) für alle \( i=1,\ldots, s \) und alle \( j=1,\ldots,r \).
	\end{description}
\end{proof}

\begin{korollar}
	Ist \( L |K \) Körpererweiterung, \( \alpha, \beta \in L \) algebraisch über \( K \), so sind auch \( \alpha+\beta, \alpha\cdot \beta \in L\) algebraisch über \( K \).
\end{korollar}
\begin{proof}
	Betrachte \(L | K[\alpha, \beta] = (K[\alpha])[\beta]| K[\alpha] | K\). Da \( \beta \) algebraisch über \( K[\alpha] \) ist, ist \( K[\alpha, \beta] \) ebenfalls ein Körper. Also ist wegen der Gradformel \[
		[K[\alpha, \beta]:K] = \underbrace{[K[\alpha, \beta]:K[\alpha]]}_{< \infty}\cdot \underbrace{[K[\alpha]:K]}_{<\infty} < \infty
	.\] Nach vorherigem Korollar ist \( K[\alpha, \beta]|K\) algebraisch, also sind \( \alpha+\beta, \alpha\cdot \beta \) algebraisch über \( K \).
\end{proof}

Eine typische Situation ist folgende:\\
\( f(X) \in K[X] \) ist irreduzibel (o.B.d.A. normiert) und \( \alpha,\beta \in L\) (wobei \( L|K \) Körpererweiterung ist) seien Nullstellen von \( f, \alpha \neq \beta \). Dann ist \( f \) das Minimalpolynom von \( \alpha \) und auch \( \beta \) über \( K \). Sei \( n:=\operatorname{deg} f\ge 2 \).

Dann ist nach vorherigem Lemma \( [K[\alpha]:K] = n \) und \( [K[\alpha,\beta]:K[\alpha]] = \operatorname{deg}(\text{MiPo von }\beta \text{ über } K[\alpha]) \).

In \( (K[\alpha])[X] \) gilt \[
	f(X) = (X-a)g(X) \text{ für ein }g(X) \in (K[\alpha])[X]
.\] Dabei ist \( \operatorname{deg}g \le n-1 \). Da \( f(\beta) = 0 \), ist \( (\beta-\alpha)g(\beta) = 0 \), also \( g(\beta) = 0 \). Das Minimalpolynom von \( \beta \) über \( K[\alpha] \) ist also ein Teiler von \( g \).

\begin{beispiel}
	\( f(X) = X^{4} - 3 \) ist irreduzibel über  \( \Q \), \( L:= \C \). Nullstellen sind  \( \alpha_i := \sqrt[4]{3}\cdot i^{j}  \) für \( j \in \{0, 1, 2, 3\}  \). Betrachte die Kette \( \C | \Q[\alpha_0, \alpha_1, \alpha_2, \alpha_3] | \Q[\alpha_0, \alpha_1, \alpha_2] | \Q[\alpha_0, \alpha_1] | \Q[\alpha_0]  | \Q \). Dabei ist \( [\Q[\alpha_0]:\Q] = 4\) nach obiger Bemerkung.

	Berechne das Minimalpolynom von \( i\sqrt[4]{3}\) in \( \Q[\sqrt[4]{3}]\):
	\[ \alpha_1^2 = -\sqrt{3} = - \alpha_0^2  .\]
	Also erfüllt \( \alpha_1 \) die Gleichung \( h(\alpha_1)=0 \) für \( h(X):= X^2+\alpha_0^2 \in \Q[\alpha_0] \). Ist \( h \) irreduzibel in \( \Q[\alpha_0][X] \), so ist \( h \) das Minimalpolynom \( \implies [\Q[\alpha_1,\alpha_0] : \Q[\alpha_0]] = 2 \). Andernfalls zerfällt \( h \) in \( \Q[\alpha_0][X] \), also ist \( \operatorname{deg}MiPo \alpha_1 < 2 \), also ist \( \Q[\alpha_0, \alpha_1] = \Q[\alpha_0] \). Das kann aber nicht passieren, denn \( \Q[\alpha_0] \subset \R \), aber \( \alpha_1 \not\in \R \). Also ist \( h \) irreduzibel und \( [\Q[\alpha_0,\alpha_1] : [\Q[\alpha_0] = 0\).

	Ferner ist \( \alpha_2 = -\alpha_0 \in \Q[\alpha_0] \subset \Q[\alpha_0, \alpha_1] \) und \( \alpha_3 = -\alpha_1 \in \Q[\alpha_1, \alpha_2] \), also ist \( \Q[\alpha_0, \alpha_1]  = \Q[\alpha_0, \alpha_1, \alpha_2, \alpha_3]\) und \( [\Q[\alpha_0, \alpha_1, \alpha_2, \alpha_3]:\Q] = 8 \).

	Wie zerfällt \( f \)? - Polynomdivision:  \[
		(X^{4}-3) : (X - \alpha_0) = X^3 + \alpha_0X^2+\alpha_0^2X + \alpha_0^3 =: g(X)
	.\] Also ist \( f(X) = (X-\alpha_0)g(X) \). Da \( \alpha_2 \in \Q[\alpha_0] \), ist \( g(X) = (X-\alpha_3)\cdot h(X) \) für ein \( h \in  \Q[\alpha_0][X] \) (nämlich obiges). Polynomdivision ergibt \[
	(X^3+\alpha_0X^2+\alpha_0^2X+\alpha_0^3) : (X+\alpha_0) = X^2+\alpha_0^2 = h(X)
.\] Also gilt in \( \Q[\alpha_0][X] \), dass \( f(X) = (X-\alpha_0)(X-\alpha_2)(X^2+\alpha_0^2) \).
\end{beispiel}
