\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{skript}
\LoadClass[a4paper,12pt,oneside,headings=optiontotoc,numbers=noenddot]{scrreprt}

\RequirePackage{scrlayer-scrpage}
\KOMAoption{pagestyleset}{standard}
\KOMAoption{markcase}{used}
\KOMAoptions{plainheadsepline, headsepline}
\automark{section}

\RequirePackage{algebra}

\newtheoremstyle{thm*}
{\topsep}
{\topsep}
{\itshape}
{0pt}
{\bfseries}
{}
{5pt plus 1pt minus 1pt}
{}

\theoremstyle{thm*}

%%%%%%%%%%%%%%%
% Präfix von Sätzen mit Zählern
\newtheorem{beispiel}{Beispiel}[chapter]
\newtheorem{bemerkung}[beispiel]{Bemerkung}
\newtheorem{satz}[beispiel]{Satz}
\newtheorem{defn}[beispiel]{Definition}
\newtheorem{korollar}[beispiel]{Korollar}
\newtheorem{lemma}[beispiel]{Lemma}
\newtheorem{theorem}[beispiel]{Theorem}
\newtheorem{defsatz}[beispiel]{Definition und Satz}
\renewenvironment{proof}{{\textbf{Beweis:} }}{\hfill $\Box$ \\}


%%%%%%%%%%%%%%%
% Präfix von Sätzen ohne Zählern
\newtheorem*{beh*}{Behauptung}
\newtheorem*{beispiel*}{Beispiel}
\newtheorem*{beg*}{Begründung}
\newtheorem*{erinnerung*}{Erinnerung}
\newtheorem*{erkenntnis*}{Erkenntnis}
\newtheorem*{frage*}{Frage}
\newtheorem*{comm*}{Kommentar}
\newtheorem*{notation*}{Notation}
\newtheorem*{notiz*}{Notiz}
\newtheorem*{problem*}{Problem}
\newtheorem*{beobachtung*}{Beobachtung}
\newtheorem*{gegeben*}{Gegeben}
\newtheorem*{bemerkung*}{Bemerkung}
\newtheorem*{gesucht*}{Gesucht}
\newtheorem*{idee*}{Idee}





